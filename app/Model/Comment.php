<?php

class Comment extends AppModel
{
    public $validate = array(
        'comment' => array(
            'required' => array(
                'rule' => 'notBlank',
                'message' => 'You need to input something for your post.',
                'last' => false
            ),
            'maximumLength' => array(
                'rule' => array('maxLength', 140),
                'message' => 'You have exceeded the allowed maximum characters of 140.'
            )
        )
    );

    public function afterFind($results, $primary = false)
    {
        foreach ($results as $key => $val) {
            if (isset($val['Comment']['comment'])) {
                $results[$key]['Comment']['comment'] = 
                h($results[$key]['Comment']['comment']);
            }
        }
        return $results;
    }
    public $actsAs = array('Containable');
    public $belongsTo = array(
        'Post' => array(
            'counterCache' => true
        ),
        'User'
    );
    // public $belongsTo = array('User');
}