<?php

class Like extends AppModel
{
    public $belongsTo = array(
        'Post' => array(
            'counterCache' => true
        ),
        'User'
    );
}