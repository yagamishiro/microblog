<?php

App::uses('AppModel', 'Model');
App::uses('BlowfishPasswordHasher', 'Controller/Component/Auth');

class User extends AppModel 
{
    public $validate = array(
        'last_name' => array(
            'required' => array(
                'rule' => 'notBlank',
                'message' => 'Last name is required.',
                'last' => false
            ),
            'alphaNumeric' => array(
                'rule' => 'alphaNumeric',
                'message' => 'Only alphabets and numbers allowed.'
            )
        ),
        'first_name' => array(
            'required' => array(
                'rule' => 'notBlank',
                'message' => 'First name is required.',
                'last' => false
            ),
            'alphaNumeric' => array(
                'rule' => 'alphaNumeric',
                'message' => 'Only alphabets and numbers allowed.'
            )
        ),
        'birth_date' => array(
            'required' => array(
                'rule' => 'notBlank',
                'message' => 'Birthdate is required.',
                'last' => false
            ),
            'validBOD' => array(
                'rule' => 'validateDOB',
                'message' => 'Invalid Date of Birth.'
            )
        ),
        'gender' => array(
            'required' => array(
                'rule' => 'notBlank',
                'message' => 'Gender is required.'
            )
        ),
        'contact_number' => array(
            'required' => array(
                'rule' => 'notBlank',
                'message' => 'Contact number is required.',
                'last' => false
            ),
            'unique' => array(
                'rule' => 'isUnique',
                'message' => 'This contact number already exist.',
                'last' => false
            ),
            'size' => array(
                'rule' => array('maxLength', 11),
                'message' => 'Contact number must not be more than 11 numbers.'
            ) 
        ),
        'email' => array(
            'required' => array(
                'rule' => 'notBlank',
                'message' => 'Email is required.',
                'last' => false
            ),
            'validEmail' => array(
                'rule' => array('email', true),
                'message' => 'Please enter a valid email address.',
                'last' => false
            ),
            'unique' => array(
                'rule' => 'isUnique',
                'message' => 'This email already exist.'
            )
        ),
        'username' => array(
            'required' => array(
                'rule' => 'notBlank',
                'message' => 'Username is required.',
                'last' => false
            ),
            'alphaNumeric' => array(
                'rule' => 'alphaNumeric',
                'message' => 'Only alphabets and numbers allowed.',
                'last' => false
            ),
            'unique' => array(
                'rule' => 'isUnique',
                'message' => 'Username has already been taken.'
            )
        ),
        'password' => array(
            'required' => array(
                'rule' => 'notBlank',
                'message' => 'Password is required.',
                'last' => false
            ),
            'size' => array(
                'rule' => array('minLength', 8),
                'message' => 'Password must have a minimum of 8 characters.'
            ) 
        ),
        'confirm_password' => array(
            'required' => array(
                'rule' => 'notBlank',
                'message' => 'Confirm password is required.',
                'last' => false
            ),
            'size' => array(
                'rule' => array('minLength', 8),
                'message' => 'Confirm password must have a minimum of 8 characters.',
                'last' => false
            ),
            'compare' => array(
                'rule' => array('validate_passwords'),
                'message' => 'The passwords you entered do not match.'
            )
        ),
        'old_password' => array(
            'required' => array(
                'rule' => 'notBlank',
                'message' => 'Current password is required.',
                'last' => false
            ),
            'compare' => array(
                'rule' => array('check_old_password'),
                'message' => 'Invalid password.'
            )
        ),
        'verification_code' => array(
            'required' => array(
                'rule' => 'notBlank',
                'message' => 'Verification Code is required.'
            )
        )
    );

    public function isOwnedBy($user)
    {
        return $this->field('id', array('id' => $user)) !== false;
    }

    public function beforeSave($options = array())
    {
        if (isset($this->data[$this->alias]['password'])) {
            $passwordHasher = new BlowfishPasswordHasher();
            $this->data[$this->alias]['password'] = $passwordHasher->hash(
                $this->data[$this->alias]['password']
            );
        }
        return true;
    }

    public function validate_passwords()
    {
        return $this->data[$this->alias]['password'] ===
        $this->data[$this->alias]['confirm_password'];
    }

    public function check_old_password()
    {
        $storedHash = $this->field('password');
        $newHash = Security::hash(
            $this->data[$this->alias]['old_password'], 'blowfish', $storedHash);
        return $storedHash == $newHash;
    }

    public function validateDOB()
    {
        if ($this->data[$this->alias]['birth_date'] > date('Y-m-d') || 
        $this->data[$this->alias]['birth_date'] == date('Y-m-d')) {
            return false;
        }
        return true;
    }

    public $actsAs = array('Containable');
    public $hasMany = array(
        'Post',
        'Comment',
        'Share',
        'Follow'
    );
}