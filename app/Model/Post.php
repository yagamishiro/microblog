<?php

class Post extends AppModel
{
    public $validate = array(
        'body' => array(
            'required' => array(
                'rule' => 'notBlank',
                'message' => 'You need to input something for your post.',
                'last' => false
            ),
            'maximumLength' => array(
                'rule' => array('maxLength', 140),
                'message' => 'You have exceeded the allowed maximum characters of 140.'
            )
        )
    );

    public function isOwnedBy($post, $user)
    {
        return $this->field('id', array('id' => $post, 'user_id' => $user)) !== false;
    }

    public function isUploadedFile($params)
    {   
        if (!empty($params['tmp_name']) && $params['tmp_name'] != 'none') {
            return true;
        }
        return false;
    }

    public function afterFind($results, $primary = false)
    {
        foreach ($results as $key => $val) {
            if (isset($val['Post']['body'])) {
                $results[$key]['Post']['body'] = h($results[$key]['Post']['body']);
            }
        }
        return $results;
    }

    public $actsAs = array('Containable');
    public $belongsTo = array('User');
    public $hasMany = array(
        'Like',
        'Comment',
        'Share'
    );
}