<div class="nav_bar">
    <div class="home_mypost mt-2">
        <p><?php echo $this->Html->link("Home", array(
            'controller' => 'posts',
            'action' => 'index'
        )); ?></p>
        <p><?php echo $this->Html->link('My Post',array(
            'controller' => 'posts',
            'action' => 'view',
            $this->Session->read('Auth.User.id')
        )); ?></p>
    </div>

    <div class="search_profile">
        <div class="row">
            <div class="col-6 mt-2">
                <?php echo $this->Form->create('Location', array('type' => 'get'));
                    echo $this->Form->input('search', array(
                        'class' => 'form-control',
                        'label' => false,
                        'placeholder' => 'Search',
                        'style' => 'height: 30px;'
                    ));
                ?>
            </div>
            <div class="col-6 mt-2">
                <p class="profile_username">
                    <?php echo $this->Session->read('Auth.User.username');?>
                    <i class='fas fa-user'></i>
                </p>
                <p>Logout <i class="fas fa-sign-out-alt"></i></p>
            </div>
        </div>
    </div>
</div>

<div class="post_container">
    <div class="create_post">
        <?php echo $this->Html->link(
        'Create Post', array(
            'posts',
            'action' => 'add'
        ), array(
            'class' => 'btn btn-outline-primary',
            'style' => 'float: right;'
        )); ?>
    </div>

    <div class="post_content">
    <?php foreach ($posts as $post): ?>
    <div class="post_cont">
    <div class="posted_by">
        <?php echo $this->Html->image('default.png', array(
            'alt' => 'CakePHP',
            'height' => 50,
            'width' => 50,
            'style' => 'border-radius: 10%;float:left;margin-right:10px;border:1px solid black;'
            )); ?>
        <b><?php echo $post['User']['first_name']; ?> 
        <?php echo $post['User']['last_name'];?></b> &#8226;
        <u class="text-muted"><?=$this->Time->format($post['Post']['modified'],
        '%H:%M %p');?> - <?=$this->Time->format($post['Post']['modified'],
        '%b %e, %Y');?></u><br>
        <i>@<?php echo $post['User']['username'];?></i>
    </div>
    <div class="post">
        <p class="text-left">
            <?php echo $post['Post']['body']; ?>
            <br>
            <?php 
                if ($post['Post']['image_src']) {
                    echo $this->Html->image('uploads/' . $post['Post']['image_src'], array(
                        'alt' => 'Image Post',
                        'height' => 300,
                        'style' => 'border-radius:10px;'
                    ));
                }
            ?>
        </p>
    </div>
    <div class="lcs_actions">
        <?= $this->Form->postLink(
                 $this->Html->tag('i', '', array(
            'class' => 'far fa-heart')), 
        array(
            'controller' => 'posts',
            'action' => 'like',
            $post['Post']['id']
        ),
        array(
            'escape' => false
        )); ?> 10

        <span><?= $this->Form->postLink(
                 $this->Html->tag('i', '', array(
            'class' => 'fas fa-comments')), 
        array(
            'controller' => 'posts',
            'action' => 'delete',
            $post['Post']['id']
        ),
        array(
            'escape' => false
        )); ?> 100</span>

        <span><?= $this->Form->postLink(
                 $this->Html->tag('i', '', array(
            'class' => 'fas fa-retweet')), 
        array(
            'controller' => 'posts',
            'action' => 'delete',
            $post['Post']['id']
        ),
        array(
            'escape' => false
        )); ?> 20</span>
    </div>
    </div>
    <?php endforeach; ?>
    <?php unset($post); ?>
    <div class="paginator">
        <p><?php
            echo $this->Paginator->prev(
        '« Previous',
        null,
        null,
        array(
            'class' => 'prev d-none'
            ));?></p>
        
        <p><?php
            echo $this->Paginator->next(
        'Next »',
        null,
        null,
        array(
            'class' => 'next d-none'
            )); ?></p>
    </div>
   </div>
</div>