<div class="login">
    <div class="row">
            <div class="col">
                <?= $this->Html->image('microblogging-icon.png', array(
                    'alt' => 'CakePHP'
                )) ?>
            </div>
            <div class="col mt-5">
                <?= $this->Flash->render('auth') ?>
                <?= $this->Form->create('User') ?>

                <h4 class="text-center text-muted mb-5">Find your Microblog account</h4>
                <fieldset>
                    <?= $this->Form->input('email', array(
                    'class' => 'form-control emailBody',
                    'label' => 'Enter your email'
                    )); ?>
                </fieldset>
                <br>
                <?= $this->Form->end(__('Submit')) ?>
                <?= $this->Html->link("Don't have an account yet? Click to Register!",
                array(
                    'controller' => 'users',
                    'action' => 'add',
                    'full_base' => true
                ), array(
                    'class' => 'font-italic decor',
                    'style' => 'color:#003d4c;'
                )) ?>
            </div>
        </div>
</div>