<div class="registration">
    <h1 class="text-center text-muted">Register</h1>
    
    <?= $this->Form->create('User') ?>
    
    <div class="row">
        <div class="col">
            <?= $this->Form->input('last_name', array('class' => 'form-control')) ?>
        </div>
        <div class="col">
            <?= $this->Form->input('first_name', array('class' => 'form-control')) ?>
        </div>
    </div>

    <div class="row">
        <div class="col">
            <?= $this->Form->input('birth_date', array(
                'type' => 'Date',
                'label' => 'Date of Birth',
                'class' => 'form-control',
                'default' => date("Y-m-d", strtotime('-24 hours', time())),
                'dateFormat' => 'DMY',
                'minYear' => date('Y') - 70,
                'maxYear' => date('y') - 1
            )) ?>
        </div>
        <div class="col">
            <?= $this->Form->input('contact_number',
            array(
                'placeholder' => 'eg. 09123456789',
                'type' => 'number',
                'class' => 'form-control')) ?>
        </div>
    </div>

    <div class="row">
        <div class="col">
            <?= $this->Form->input('email', array(
                'type' => 'email',
                'class' => 'form-control')) ?>
        </div>
        <div class="col">
            <?= $this->Form->input('username', array(
                'class' => 'form-control'
            )) ?>
        </div>
    </div>

    <div class="row">
        <div class="col">
            <?= $this->Form->input('password', array(
                'class' => 'form-control'
            )) ?>
        </div>
        <div class="col">
            <?= $this->Form->input('confirm_password', array(
                'type' => 'password',
                'class' => 'form-control'
            )) ?>
        </div>
    </div>
    
    <div class="row">
        <div class="col">
        <?= $this->Form->end(__('Register')) ?>
        </div>
    </div>
</div>