<div class="login">
    <div class="row">
            <div class="col">
                <?= $this->Html->image('microblogging-icon.png', array(
                    'alt' => 'CakePHP'
                )) ?>
            </div>
            <div class="col mt-5">
                <?= $this->Flash->render('auth') ?>
                <?= $this->Form->create('User') ?>

                <h1 class="text-center text-muted">Login</h1>
                
                <fieldset>
                    <?= $this->Form->input('username', array(
                    'class' => 'form-control'
                    )); ?>
                    <?= $this->Form->input('password', array(
                    'class' => 'form-control'
                    )) ?>
                </fieldset>
                <?= $this->Html->link('Forgot Password?', array(
                    'controller' => 'users',
                    'action' => 'forgot_password'
                    ), array(
                    'class' => 'float-right',
                    'style' => 'color:#003d4c;',
                    'type' => 'button'

                    )) ?>
                <br><br>
                
                <?= $this->Form->end(__('Login')) ?>
                <?= $this->Html->link("Don't have an account yet? Click to Register!",
                array(
                    'controller' => 'users',
                    'action' => 'add',
                    'full_base' => true
                ), array(
                    'class' => 'font-italic decor',
                    'style' => 'color:#003d4c;'
                )) ?>
            </div>
        </div>
</div>