<div class="login">
    <div class="row">
            <div class="col">
                <?= $this->Html->image('microblogging-icon.png', array(
                    'alt' => 'CakePHP'
                )) ?>
            </div>
            <div class="col mt-5">
                <?= $this->Form->create('User') ?>

                <h4 class="text-center text-muted mb-5">Reset Password</h4>
                <fieldset>
                <?= $this->Form->input('password', array(
                'label' => 'New Password',
                'class' => 'form-control'
                )) ?>
                <?= $this->Form->input('confirm_password', array(
                'type' => 'password',
                'label' => 'Confirm Password',
                'class' => 'form-control'
                )) ?>
                </fieldset>
                <br>
                <?= $this->Form->end(__('Reset Password')) ?>
            </div>
        </div>
</div>