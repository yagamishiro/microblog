<div class="nav_bar">
    <div class="home_mypost mt-2">
        <p>
            <?= $this->Html->link("Home", array(
            'controller' => 'posts',
            'action' => 'index'
            )) ?>
        </p>
        <?php if ($this->Session->read('Auth.User.id')) { ?>
        <p>
            <?= $this->Html->link('My Post',array(
            'controller' => 'posts',
            'action' => 'view',
            $this->Session->read('Auth.User.id')
            )) ?></p>
        <?php } ?>
    </div>

    <div class="search_profile">
        <div class="row">
            <div class="col-6 mt-2">
            <?= $this->Form->create('Search', array(
                'url' => array(
                    'controller' => 'searches',
                    'action' => 'search'
                ))) ?>
            <?= $this->Form->input('keywords', array(
                'class' => 'form-control',
                'label' => false,
                'placeholder' => 'Search',
                'style' => 'height: 30px;',
                'required' => true
                )) ?>
            <?= $this->Form->end() ?>
            </div>
            <div class="col-6 mt-2">
                <p class="profile_username">
                    <?= $this->Html->link(
                        $this->Session->read('Auth.User.username'), array(
                            'controller' => 'users',
                            'action' => 'view',
                            $this->Session->read('Auth.User.id')
                        )) ?>
                    <i class='fas fa-user'></i>
                </p>
                <p><?= $this->Html->link(
                    'Logout', array(
                        'controller' => 'users',
                        'action' => 'logout'
                    )) ?>
                    <i class="fas fa-sign-out-alt"></i>
                </p>
            </div>
        </div>
    </div>
</div>

<div class="edit_profile">
    <div class="edit_profile_header">
        <p>Update Your Profile</p>
    </div>
    <?php if ($user['User']['status'] != 0) { ?>
    <div class="edit_profile_tabs mt-1">
    <p>
        <?= $this->Html->link('Profile', array(
            'controller' => 'users',
            'action' => 'edit',
            $this->Session->read('Auth.User.id')
        )) ?>
    </p>
    <p>
        <?= $this->Html->link('Change Password', array(
            'controller' => 'users',
            'action' => 'change_password',
            $this->Session->read('Auth.User.id')
        )) ?>
    </p>
    </div>

    <!-- Edit Profile Information Form -->
    <?= $this->Form->create('User', array(
        'enctype' => 'multipart/form-data',
        'class' => 'edit_profile_form'
        )) ?>
   <br>
   <label class="font-weight-bold text-muted">Profile Picture</label>
   <br>
    <?php 
        if ($user['User']['profile_picture']) {
            if ($user['User']['profile_picture'] == 'default.png') {
            echo $this->Html->image($user['User']['profile_picture'], array(
                'alt' => 'Image Post',
                'height' => 200,
                'width' => 200,
                'id' => 'image_src'
            ));
            } else {
            echo $this->Html->image('uploads/' . $user['User']['profile_picture'], array(
                'alt' => 'Image Post',
                'height' => 200,
                'width' => 200,
                'id' => 'image_src'
            ));
            }
        }
    ?>
    <div class="preview_img">
        <img id="prevImg" />
    <br>
    
    <?= $this->Form->button('Remove', array(
            'type' => 'button',
            'class' => 'btn btn-outline-danger mt-2',
            'style' => 'margin-left: 15%;',
            'id' => 'remove_image'
            )) ?>
    </div>
    <br>
    
    <?= $this->Form->File('image', array(
        'accept' => 'image/*',
        'style' => 'width:20%;',
        'id' => 'file'
    )) ?>
    
    <div class="row">
    <div class="col-6">
        <?= $this->Form->input('last_name', array(
        'label' => 'Last Name',
        'class' => 'form-control'
        )) ?>
    </div>
    <div class="col-6">
        <?= $this->Form->input('first_name', array(
        'label' => 'First Name',
        'class' => 'form-control'
        )) ?>
    </div>
    </div>
    
    <div class="row">
    <div class="col-6">
        <?= $this->Form->input('birth_date', array(
        'type' => 'Date',
        'label' => 'Date of Birth',
        'class' => 'form-control',
        'dateFormat' => 'DMY',
        'readonly'
        )) ?>
        </div>
    <div class="col-6">
        <?= $this->Form->input('contact_number', array(
        'label' => 'Contact Number',
        'class' => 'form-control'
        )) ?>
    </div>
    </div>
    
    <div class="row">
    <div class="col-6">
        <?= $this->Form->input('username', array(
        'label' => 'Username',
        'class' => 'form-control'
        )) ?>
    </div>
    <div class="col-6">
        <?= $this->Form->input('email', array(
        'label' => 'Email',
        'class' => 'form-control'
        )) ?>
    </div>
    </div>

    <?= $this->Form->input('id', array('type' => 'hidden')) ?>
    <br>
    <?= $this->Form->end('Update Profile') ?>
    <?php } else { ?>
    <div class="edit_profile_form">
        <div class="text-muted text-center">
        A verification code has been sent to your new email address.
        <br>
        Please check your email for the code.
        </div>
        <br>
        <?= $this->Form->create('User', array(
            'url' => array(
                'controller' => 'users',
                'action' => 'reactivate'
            )
        )) ?>
        <div class="col-8" style="margin:auto;">
            <?= $this->Form->input('verification_code', array(
            'label' => false,
            'class' => 'form-control',
            'placeholder' => 'Enter code here'
            )) ?>
        </div>
        <br>
        <?= $this->Form->input('id', array('type' => 'hidden')) ?>
        <?= $this->Form->end('Submit') ?>
    </div>
    <?php } ?>
</div>

<script>
$('.preview_img').hide();

$("#file").change(function () {
    $('#image_src').hide();
    filePreview(this);
});

$('#remove_image').click(function() {
    $("#file").val(null);
    $('.preview_img').hide();
    $('#image_src').show();
});

function filePreview(input)
{
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function (e) {
            $('.preview_img').show();
            $('#prevImg').attr('src', e.target.result);
        };
        reader.readAsDataURL(input.files[0]);
    }
}
</script>