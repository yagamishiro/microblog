<div class="nav_bar">
    <div class="home_mypost mt-2">
        <p>
            <?= $this->Html->link("Home", array(
            'controller' => 'posts',
            'action' => 'index'
            )) ?>
        </p>
        <?php if ($this->Session->read('Auth.User.id')) { ?>
        <p>
            <?= $this->Html->link('My Post',array(
            'controller' => 'posts',
            'action' => 'view',
            $this->Session->read('Auth.User.id')
            )) ?></p>
        <?php } ?>
    </div>

    <div class="search_profile">
        <div class="row">
            <div class="col-6 mt-2">
            <?= $this->Form->create('Search', array(
                'url' => array(
                    'controller' => 'searches',
                    'action' => 'search'
                ))) ?>
            <?= $this->Form->input('keywords', array(
                'class' => 'form-control',
                'label' => false,
                'placeholder' => 'Search',
                'style' => 'height: 30px;',
                'required' => true
                )) ?>
            <?= $this->Form->end() ?>
            </div>
            <div class="col-6 mt-2">
                <p class="profile_username">
                    <?= $this->Html->link(
                        $this->Session->read('Auth.User.username'), array(
                            'controller' => 'users',
                            'action' => 'view',
                            $this->Session->read('Auth.User.id')
                        )) ?>
                    <i class='fas fa-user'></i>
                </p>
                
                <p>
                    <?= $this->Html->link(
                    'Logout', array(
                        'controller' => 'users',
                        'action' => 'logout'
                    )) ?>
                    <i class="fas fa-sign-out-alt"></i>
                </p>

            </div>
        </div>
    </div>
</div>

<div class="edit_profile">
    <div class="edit_profile_header">
        <p>Update Your Password</p>
    </div>
    <div class="edit_profile_tabs mt-1">
    <p>
        <?= $this->Html->link('Profile', array(
            'controller' => 'users',
            'action' => 'edit',
            $this->Session->read('Auth.User.id')
        )) ?>
    </p>

    <p>
        <?= $this->Html->link('Change Password', array(
            'controller' => 'users',
            'action' => 'change_password',
            $this->Session->read('Auth.User.id')
        )) ?>
    </p>
    </div>

    <!-- Edit Password Form -->
    <?= $this->Form->create('User', array(
        'enctype' => 'multipart/form-data',
        'class' => 'edit_profile_form',
        'id' => 'password_form'
        )) ?>
    
    <?= $this->Form->input('old_password', array(
        'label' => 'Current Password',
        'type' => 'password',
        'class' => 'form-control col-10'
        )) ?>

    <div class="row">
    <div class="col-5">
        <?= $this->Form->input('password', array(
        'label' => 'New Password',
        'class' => 'form-control'
        )) ?>
    </div>
    <div class="col-5">
        <?= $this->Form->input('confirm_password', array(
        'type' => 'password',
        'label' => 'Confirm Password',
        'class' => 'form-control'
        )) ?>
    </div>
    </div>

    <?= $this->Form->input('id', array('type' => 'hidden')) ?>
    <br>
    <?= $this->Form->end('Update Password') ?>
</div>

<script>
$('.preview_img').hide();

$("#file").change(function () {
    $('#image_src').hide();
    filePreview(this);
});

$('#remove_image').click(function() {
    $("#file").val(null);
    $('.preview_img').hide();
    $('#image_src').show();
});

function filePreview(input)
{
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function (e) {
            $('.preview_img').show();
            $('#prevImg').attr('src', e.target.result);
        };
        reader.readAsDataURL(input.files[0]);
    }
}
</script>