<div class="nav_bar">
    <div class="home_mypost mt-2">
        <p>
            <?= $this->Html->link("Home", array(
            'controller' => 'posts',
            'action' => 'index'
            )) ?>
        </p>
        <?php if ($this->Session->read('Auth.User.id')) { ?>
        <p>
            <?= $this->Html->link('My Post',array(
            'controller' => 'posts',
            'action' => 'view',
            $this->Session->read('Auth.User.id')
            )) ?></p>
        <?php } ?>
    </div>

    <div class="search_profile">
        <div class="row">
            <div class="col-6 mt-2">
            <?= $this->Form->create('Search', array(
                'url' => array(
                    'controller' => 'searches',
                    'action' => 'search'
                ))) ?>
            <?= $this->Form->input('keywords', array(
                'class' => 'form-control',
                'label' => false,
                'placeholder' => 'Search',
                'style' => 'height: 30px;',
                'required' => true
                )) ?>
            <?= $this->Form->end() ?>
            </div>
            <div class="col-6 mt-2">
                <p class="profile_username">
                    <?= $this->Html->link(
                        $this->Session->read('Auth.User.username'), array(
                            'controller' => 'users',
                            'action' => 'view',
                            $this->Session->read('Auth.User.id')
                        )) ?>
                    <i class='fas fa-user'></i>
                </p>
                <p>
                    <?= $this->Html->link(
                    'Logout', array(
                        'controller' => 'users',
                        'action' => 'logout'
                    )) ?>
                    <i class="fas fa-sign-out-alt"></i>
                </p>
            </div>
        </div>
    </div>
</div>

<div class="profile_container">
    <div class="left_profile">
        <div class="profile_pic">
            <?php if ($user['User']['profile_picture'] == 'default.png') { ?>
            <?= $this->Html->image(
            $user['User']['profile_picture'], array(
            'alt' => 'Profile Picture',
            'class' => 'profile_pic'
            )) ?>
            <?php } else { ?>
            <?= $this->Html->image(
            'uploads/' . $user['User']['profile_picture'], array(
            'alt' => 'Profile Picture',
            'class' => 'profile_pic'
            )) ?>
            <?php } ?>
        </div>
        <div class="profile_info">
            <h5 id="profile_name">
                <?= $user['User']['first_name']. ' ' .$user['User']['last_name'] ?>
            </h5>

            <p class="username text-muted">@<?= $user['User']['username'] ?></p>
            <br><br>
            <p>
                <i class="far fa-envelope"></i> <?= $user['User']['email'] ?>
            </p>
            <br>
            <p>
                <i class="fas fa-birthday-cake"></i> 
                <?= $this->Time->format(
                $user['User']['birth_date'], '%B %e, %Y') ?>
            </p>
            <br><br>
            <p>
                <i class="far fa-calendar-alt"></i> Joined on 
                <?= $this->Time->format(
                $user['User']['created'], '%B %Y') ?>
            </p>
        </div>
        <?php if ($user['User']['id'] == $this->Session->read('Auth.User.id')) {?>
        <div class="edit_button">
            <?= $this->Html->link('Edit Profile', array(
                'controller' => 'users',
                'action' => 'edit',
                $this->Session->read('Auth.User.id')
            ), array(
                'class' => 'btn btn-outline-primary btn-block',
                'style' => 'margin:auto;'
            )) ?>
        </div>
        <?php } elseif ($user['Count']['followed_by_me'] >= 1)  { ?>
        <div class="edit_button">
            <?= $this->Form->postLink('Unfollow', array(
                'controller' => 'users',
                'action' => 'unfollow',
                $user['User']['id']
            ), array(
                'class' => 'btn btn-outline-primary btn-block',
                'style' => 'margin:auto;'
            )) ?>
        </div>
        <?php } else {?>
        <div class="edit_button">
            <?= $this->Form->postLink('Follow', array(
                'controller' => 'users',
                'action' => 'follow',
                $user['User']['id']
            ), array(
                'class' => 'btn btn-outline-primary btn-block',
                'style' => 'margin:auto;'
            )) ?>
        </div>
        <?php } ?>
    </div>
    <div class="right_profile">
    <div class="follow_following_title">
        <p>Posts: <b><?= $user['Count']['post_count'] ?></b></p>
        <p>Followers: <b><?= $user['Count']['followers'] ?></b></p>
        <p>Following: <b><?= $user['User']['follow_count'] ?></b></p>
    </div>
    <div class="follow_following_title">
        <p>
            <?= $this->Html->link(
            'Posts', array(
            'controller' => 'users',
            'action' => 'view',
            $user['User']['id']
            )) ?>
        </p>
        <p>
            <?= $this->Html->link(
            'Followers', array(
            'controller' => 'users',
            'action' => 'view_follower',
            $user['User']['id']
            )) ?>
        </p>
        <p>
            <?php echo $this->Html->link(
            'Following', array(
            'controller' => 'users',
            'action' => 'view_following',
            $user['User']['id']), array(
            'style' => 'text-decoration: underline;'
            )) ?>
        </p>
    </div>
    <div class="followers_list">
        <br>
        <?php if ($followings) { ?>
        <?php foreach ($followings as $following):?>
        <div>
        <div class="follow_cont row">
            <div class="followers_name col-9">
                <?php if ($following['followed_by_me_info']['User']['profile_picture']
                == 'default.png') { ?>
                <?= $this->Html->image(
                $following['followed_by_me_info']['User']['profile_picture'],
                array(
                'alt' => 'CakePHP',
                'height' => 50,
                'width' => 50,
                'style' => 
                'border-radius: 10%;float:left;margin-right:10px;border:1px solid black;'
                )) ?>
                <?php } else { ?>
                <?= $this->Html->image('uploads/' . 
                $following['followed_by_me_info']['User']['profile_picture'],
                array(
                'alt' => 'CakePHP',
                'height' => 50,
                'width' => 50,
                'style' => 
                'border-radius: 10%;float:left;margin-right:10px;border:1px solid black;'
                )) ?>
                <?php } ?>
            <b>
                <?= $this->Html->link(
                $following['followed_by_me_info']['User']['full_name'],
                array(
                    'controller' => 'users',
                    'action' => 'view',
                    $following['followed_by_me_info']['User']['id']
                ),array(
                    'style' => 'color:#003d4c;'
                )) ?>
            </b>
            <br>
            
            <i>@<?= $following['followed_by_me_info']['User']['username'] ?></i>
            </div>
            
            <?php if ($following['Follow']['following'] == $this->Session->read('Auth.User.id')) { ?>
            <?php } elseif ($following['followed_by_me']) { ?>
            <?= $this->Form->postLink('Unfollow', array(
                'controller' => 'users',
                'action' => 'unfollow',
                $following['Follow']['following']
            ), array(
                'class' => 'btn btn-outline-primary col-2',
                'style' => 'margin:auto;'
                )
            ) ?>
            <?php } else { ?>
            <?= $this->Form->postLink('Follow', array(
                'controller' => 'users',
                'action' => 'follow',
                $following['User']['id']
            ), array(
                'class' => 'btn btn-outline-primary col-2',
                'style' => 'margin:auto;'
                )
            ) ?>
            <?php } ?>
        </div>
        </div>
        <hr>
        <?php endforeach; ?>
        <?php unset($following); ?>
        <?php } else { ?>
        <div class="follow_cont text-muted font-italic text-center">
            <h3>No followings yet</h3>
        </div>
        <?php } ?>
    </div>
</div>
</div>