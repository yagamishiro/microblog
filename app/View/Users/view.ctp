<div class="nav_bar">
    <div class="home_mypost mt-2">
        <p>
            <?= $this->Html->link("Home", array(
            'controller' => 'posts',
            'action' => 'index'
            )) ?>
        </p>
        <?php if ($this->Session->read('Auth.User.id')) { ?>
        <p>
            <?= $this->Html->link('My Post',array(
            'controller' => 'posts',
            'action' => 'view',
            $this->Session->read('Auth.User.id')
            )) ?></p>
        <?php } ?>
    </div>

    <div class="search_profile">
        <div class="row">
            <div class="col-6 mt-2">
            <?= $this->Form->create('Search', array(
                'url' => array(
                    'controller' => 'searches',
                    'action' => 'search'
                ))) ?>
            <?= $this->Form->input('keywords', array(
                'class' => 'form-control',
                'label' => false,
                'placeholder' => 'Search',
                'style' => 'height: 30px;',
                'required' => true
                )) ?>
            <?= $this->Form->end() ?>
            </div>
            <?php if ($this->Session->read('Auth.User.id')) { ?>
            <div class="col-6 mt-2">
                <p class="profile_username">
                    <?= $this->Html->link(
                        $this->Session->read('Auth.User.username'), array(
                            'controller' => 'users',
                            'action' => 'view',
                            $this->Session->read('Auth.User.id')
                        ), array(
                            'style' => 'text-decoration: underline;'
                        )) ?>
                    <i class='fas fa-user'></i>
                </p>
                <p>
                    <?= $this->Html->link(
                    'Logout', array(
                        'controller' => 'users',
                        'action' => 'logout'
                    )) ?>
                    <i class="fas fa-sign-out-alt"></i>
                </p>
            </div>
            <?php } else { ?>
            <div class="col-6 mt-2">
            <p class="profile_username">
                <?= $this->Html->link(
                    'Login/Register', array(
                        'controller' => 'users',
                        'action' => 'login',
                    )) ?>
                <i class="fas fa-sign-in-alt"></i>
            </p>
            </div>
            <?php } ?>
        </div>
    </div>
</div>

<div class="profile_container">
    <div class="left_profile">
        <div class="profile_pic">
        <?php if ($user['User']['profile_picture'] == 'default.png') { ?>
            <?= $this->Html->image(
                $user['User']['profile_picture'], array(
                'alt' => 'Profile Picture',
                'class' => 'profile_pic'
            )) ?>
        <?php } else { ?>
            <?= $this->Html->image(
                'uploads/' . $user['User']['profile_picture'], array(
                'alt' => 'Profile Picture',
                'class' => 'profile_pic'
            )) ?>
        <?php } ?>
        </div>
        <div class="profile_info">
            <h5 id="profile_name">
                <?= $user['User']['first_name']. ' ' .$user['User']['last_name'] ?>
            </h5>
            <p class="username text-muted">@<?= $user['User']['username'] ?></p>
            <br><br>
            <p>
                <i class="far fa-envelope"></i> <?=$user['User']['email'] ?>
            </p>
            <br>
            <p>
                <i class="fas fa-birthday-cake"></i> 
                <?=$this->Time->format(
                $user['User']['birth_date'], '%B %e, %Y')?>
            </p>
            <br><br>
            <p>
                <i class="far fa-calendar-alt"></i> Joined on 
                <?= $this->Time->format(
                $user['User']['created'], '%B %Y') ?>
            </p>
        </div>
        <?php if ($user['User']['id'] == $this->Session->read('Auth.User.id')) {?>
        <div class="edit_button">
            <?= $this->Html->link('Edit Profile', array(
                'controller' => 'users',
                'action' => 'edit',
                $this->Session->read('Auth.User.id')
            ), array(
                'class' => 'btn btn-outline-primary btn-block',
                'style' => 'margin:auto;'
            )) ?>
        </div>
        <?php } elseif ($user['Count']['followed_by_me'] >= 1)  { ?>
        <div class="edit_button">
            <?= $this->Form->postLink('Unfollow', array(
                'controller' => 'users',
                'action' => 'unfollow',
                $user['User']['id']
            ), array(
                'class' => 'btn btn-outline-primary btn-block',
                'style' => 'margin:auto;'
            )) ?>
        </div>
        <?php } else {?>
        <div class="edit_button">
            <?= $this->Form->postLink('Follow', array(
                'controller' => 'users',
                'action' => 'follow',
                $user['User']['id']
            ), array(
                'class' => 'btn btn-outline-primary btn-block',
                'style' => 'margin:auto;'
            )) ?>
        </div>
        <?php } ?>
    </div>
    <div class="right_profile">
    <div class="follow_following_title">
        <p>Posts: <b><?= $user['Count']['post_count'] ?></b></p>
        <p>Followers: <b><?= $user['Count']['followers'] ?></b></p>
        <p>Following: <b><?= $user['User']['follow_count'] ?></b></p>
    </div>
    <div class="follow_following_title">
        <p>
            <?= $this->Html->link(
            'Posts', array(
            'controller' => 'users',
            'action' => 'view',
            $user['User']['id']), array(
            'style' => 'text-decoration: underline;'
            )) ?>
        </p>
        <p>
            <?= $this->Html->link(
            'Followers', array(
            'controller' => 'users',
            'action' => 'view_follower',
            $user['User']['id']
            )) ?>
        </p>
        <p>
            <?php echo $this->Html->link(
            'Following', array(
            'controller' => 'users',
            'action' => 'view_following',
            $user['User']['id']
            )) ?>
        </p>
    </div>
    <div class="followers_list">
        <br>
        <?php if ($posts) { ?>
        <?php foreach ($posts as $post):?>
        <div>
        <div>
        <?php if ($post['User']['profile_picture'] == 'default.png') { ?>
        <?= $this->Html->image($post['User']['profile_picture'],
        array(
            'alt' => 'CakePHP',
            'height' => 50,
            'width' => 50,
            'style' => 'border-radius: 10%;float:left;margin-right:10px;border:1px solid black;'
            )) ?>
        <?php } else { ?>
        <?= $this->Html->image('uploads/' . $post['User']['profile_picture'],
        array(
            'alt' => 'CakePHP',
            'height' => 50,
            'width' => 50,
            'style' => 'border-radius: 10%;float:left;margin-right:10px;border:1px solid black;'
            )) ?>
        <?php } ?>
        <b>
            <?= $this->Html->link(
            $post['User']['full_name'], array(
                'controller' => 'users',
                'action' => 'view',
                $post['User']['id']
            ),array(
                'style' => 'color:#003d4c;'
            )) ?>
        </b>
        <i>@<?= $post['User']['username'] ?></i>
        <!-- &#8226; -->
        <u class="text-muted float-right">
        <?php
                if (strtotime('-365 day') > strtotime($post['Post']['modified'])) {
                    echo $this->Time->timeAgoInWords($post['Post']['modified'],
                    array(
                    'accuracy' => array('year' => 'year'),
                    'end' => '1 year'));
                } elseif (strtotime('-31 day') > strtotime($post['Post']['modified'])) {
                    echo $this->Time->timeAgoInWords($post['Post']['modified'],
                    array(
                    'accuracy' => array('month' => 'month'),
                    'end' => '1 year'));
                } elseif (strtotime('-7 day') > strtotime($post['Post']['modified'])) {
                    echo $this->Time->timeAgoInWords($post['Post']['modified'],
                    array(
                    'accuracy' => array('week' => 'week'),
                    'end' => '1 year'));
                } elseif (strtotime('-1 day') > strtotime($post['Post']['modified'])) {
                    echo $this->Time->timeAgoInWords($post['Post']['modified'],
                    array(
                    'accuracy' => array('day' => 'day'),
                    'end' => '1 year'));
                } elseif (strtotime('-1 hour') > strtotime($post['Post']['modified'])) {
                    echo $this->Time->timeAgoInWords($post['Post']['modified'],
                    array(
                    'accuracy' => array('hour' => 'hour'),
                    'end' => '1 year'));
                } else {
                    echo $this->Time->timeAgoInWords($post['Post']['modified'],
                    array(
                    'end' => '1 year'));
                }
            ?>
        </u>
        <br>
        </div>
        <div class="post">
        <p class="text-left">
            <?= $post['Post']['body'] ?>
            <br>
            <?php 
                if ($post['Post']['image_src']) {
                    echo $this->Html->image('uploads/' . $post['Post']['image_src'], array(
                        'alt' => 'Image Post',
                        'height' => 300,
                        'style' => 'border-radius:10px;'
                    ));
                }
            ?>
        </p>
        <br>
       <?php 
        if ($post['Post']['is_a_share'] == 1) {
        for ($i=0;$i<count($shares);$i++) {
        if ($post['Post']['shared_id'] == $shares[$i]['Share']['id']) { ?>
        <div class="shared_cont">
        <div class="posted_by">
            <?php if ($shares[$i]['original_user_post']['User']['profile_picture']
            == 'default.png') { ?>
            <?= $this->Html->image( 
            $shares[$i]['original_user_post']['User']['profile_picture'],
            array(
                'alt' => 'CakePHP',
                'height' => 50,
                'width' => 50,
                'style' => 'border-radius: 10%;float:left;
                    margin-right:10px;border:1px solid black;'
                )) ?>
            <?php } else { ?>
            <?= $this->Html->image('uploads/' . 
            $shares[$i]['original_user_post']['User']['profile_picture'],
            array(
                'alt' => 'CakePHP',
                'height' => 50,
                'width' => 50,
                'style' => 'border-radius: 10%;float:left;
                    margin-right:10px;border:1px solid black;'
                )) ?>
            <?php } ?>
            <b>
                <?= $this->Html->link(
                $shares[$i]['original_user_post']['User']['full_name'],
                array(
                    'controller' => 'users',
                    'action' => 'view',
                    $shares[$i]['original_user_post']['User']['id']
                ), array(
                    'style' => 'color:#003d4c;'
                )) ?>
            </b> &#8226;
            
            <u class="text-muted">
                <?php
                if (strtotime('-365 day') > strtotime($shares[$i]['Post']['modified'])) {
                    echo $this->Time->timeAgoInWords($shares[$i]['Post']['modified'],
                    array(
                    'accuracy' => array('year' => 'year'),
                    'end' => '1 year'));
                } elseif (strtotime('-30 day') > strtotime($shares[$i]['Post']['modified'])) {
                    echo $this->Time->timeAgoInWords($shares[$i]['Post']['modified'],
                    array(
                    'accuracy' => array('month' => 'month'),
                    'end' => '1 year'));
                } elseif (strtotime('-7 day') > strtotime($shares[$i]['Post']['modified'])) {
                    echo $this->Time->timeAgoInWords($shares[$i]['Post']['modified'],
                    array(
                    'accuracy' => array('week' => 'week'),
                    'end' => '1 year'));
                } elseif (strtotime('-1 day') > strtotime($shares[$i]['Post']['modified'])) {
                    echo $this->Time->timeAgoInWords($shares[$i]['Post']['modified'],
                    array(
                    'accuracy' => array('day' => 'day'),
                    'end' => '1 year'));
                } elseif (strtotime('-1 hour') > strtotime($shares[$i]['Post']['modified'])) {
                    echo $this->Time->timeAgoInWords($shares[$i]['Post']['modified'],
                    array(
                    'accuracy' => array('hour' => 'hour'),
                    'end' => '1 year'));
                } else {
                    echo $this->Time->timeAgoInWords($shares[$i]['Post']['modified'],
                    array(
                    'end' => '1 year'));
                }
            ?>
            </u>
            <br>
            
            <i>@<?= $shares[$i]['original_user_post']['User']['username'] ?></i>
        </div>
        <div class="post">
        <p class="text-left">
            <?= $shares[$i]['Post']['body'] ?>
            <br>
            <?php 
                if ($shares[$i]['Post']['image_src']) {
                    echo $this->Html->image('uploads/' . $shares[$i]['Post']['image_src'], array(
                        'alt' => 'Image Post',
                        'height' => 300,
                        'style' => 'border-radius:10px;'
                    ));
                }
            ?>
        </p>
        </div>
        </div>
        <?php }}} ?>
        <hr>
        <?php endforeach; ?>
        <?php unset($follower); ?>
        <div class="paginator">
        <p>
            <?= $this->Paginator->prev(
            '« Previous',
            null,
            null, array(
            'class' => 'prev d-none'
            )) ?>
        </p>
        
        <p>
            <?= $this->Paginator->next(
            'Next »',
            null,
            null, array(
            'class' => 'next d-none'
            )) ?>
        </p>
        </div>
        <?php } else { ?>
        <div>
        <div class="follow_cont text-muted font-italic text-center">
            <h3>No posts yet</h3>
        </div>
        </div>
        <?php } ?>
    </div>
</div>
</div>