<hr>
<div>
    <p>Comments</p>
    <?php if ($comments) {?>
        <?php foreach ($comments as $comment): ?>
            <div class="comment_cont">
            <div class="posted_by comment_by">
                <?php if ($comment['User']['profile_picture'] == 'default.png') { ?>
                <?= $this->Html->image(
                $comment['User']['profile_picture'],
                array(
                'alt' => 'CakePHP',
                'height' => 30,
                'width' => 30,
                'style' => 'border-radius: 30%;float:left;
                    margin-right:10px;border:1px solid black;'
                )) ?>
                <?php } else { ?>
                <?= $this->Html->image(
                'uploads/' . $comment['User']['profile_picture'],
                array(
                'alt' => 'CakePHP',
                'height' => 30,
                'width' => 30,
                'style' => 'border-radius: 30%;float:left;
                    margin-right:10px;border:1px solid black;'
                )) ?>
               <?php } ?>
                <b>
                    <?= $this->Html->link(
                    $comment['User']['full_name'], array(
                        'controller' => 'users',
                        'action' => 'view',
                        $comment['User']['id']
                    ),array(
                        'style' => 'color:#003d4c;'
                    )) ?> 
                </b> &#8226;
            
                <u class="text-muted font-italic">
                <?php
                if (strtotime('-365 day') > strtotime($comment['Comment']['modified'])) {
                    echo $this->Time->timeAgoInWords($comment['Comment']['modified'],
                    array(
                    'accuracy' => array('year' => 'year'),
                    'end' => '1 year'));
                } elseif (strtotime('-31 day') > strtotime($comment['Comment']['modified'])) {
                    echo $this->Time->timeAgoInWords($comment['Comment']['modified'],
                    array(
                    'accuracy' => array('month' => 'month'),
                    'end' => '1 year'));
                } elseif (strtotime('-7 day') > strtotime($comment['Comment']['modified'])) {
                    echo $this->Time->timeAgoInWords($comment['Comment']['modified'],
                    array(
                    'accuracy' => array('week' => 'week'),
                    'end' => '1 year'));
                } elseif (strtotime('-1 day') > strtotime($comment['Comment']['modified'])) {
                    echo $this->Time->timeAgoInWords($comment['Comment']['modified'],
                    array(
                    'accuracy' => array('day' => 'day'),
                    'end' => '1 year'));
                } elseif (strtotime('-1 hour') > strtotime($comment['Comment']['modified'])) {
                    echo $this->Time->timeAgoInWords($comment['Comment']['modified'],
                    array(
                    'accuracy' => array('hour' => 'hour'),
                    'end' => '1 year'));
                } else {
                    echo $this->Time->timeAgoInWords($comment['Comment']['modified'],
                    array(
                    'end' => '1 year'));
                }
                ?>
                </u>
                <!-- <hr> -->
            </div>
                <p class="comment_body">
                    <?= $comment['Comment']['comment'] ?>
                </p>
            </div>
        <?php endforeach; ?>
    <?php unset($comment); } else { ?>
        <div class="comment_cont">
            <i>No comments yet. Be the first to write a comment.</i>
        </div>
    <?php }?>
</div>