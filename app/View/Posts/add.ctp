<div class="nav_bar">
    <div class="home_mypost mt-2">
    <p>
        <?= $this->Html->link("Home", array(
        'controller' => 'posts',
        'action' => 'index'
        )) ?>
    </p>
    <?php if ($this->Session->read('Auth.User.id')) { ?>
        <p>
            <?= $this->Html->link('My Post',array(
            'controller' => 'posts',
            'action' => 'view',
            $this->Session->read('Auth.User.id')
            )) ?></p>
        <?php } ?>
    </div>

    <div class="search_profile">
        <div class="row">
        <div class="col-6 mt-2">
            <?= $this->Form->create('Search', array(
                'url' => array(
                    'controller' => 'searches',
                    'action' => 'search'
                ))) ?>
            <?= $this->Form->input('keywords', array(
                'class' => 'form-control',
                'label' => false,
                'placeholder' => 'Search',
                'style' => 'height: 30px;',
                'required' => true
                )) ?>
            <?= $this->Form->end() ?>
        </div>
        <div class="col-6 mt-2">
            <p class="profile_username">
                <?= $this->Html->link(
                $this->Session->read('Auth.User.username'), array(
                'controller' => 'users',
                'action' => 'view',
                $this->Session->read('Auth.User.id')
                )) ?>
                <i class='fas fa-user'></i>
            </p>
            <p>
                <?= $this->Html->link(
                'Logout', array(
                'controller' => 'users',
                'action' => 'logout'
                )) ?>
                <i class="fas fa-sign-out-alt"></i>
            </p>
        </div>
        </div>
    </div>
</div>

<div class="create_post_container">
    <div class="create_post_header">
        <p class="create_post_title">What are your thoughts?</p>
    </div>

    <?= $this->Form->create('Post', array(
    'enctype' => 'multipart/form-data',
    'id' => 'uploadForm'
    )) ?>

    <?= $this->Form->input('body', array(
    'rows' => '7',
    'label' => false,
    'class' => 'post_body'
    )) ?>

    <div class="preview_image">
        <img id="prev_img" />
        <br>
        <?= $this->Form->button('Remove', array(
        'type' => 'button',
        'class' => 'btn btn-outline-danger mt-2',
        'style' => 'margin-left: 45%;',
        'id' => 'remove_image'
        )) ?>
    </div>

    <?= $this->Form->File('image', array(
    'accept' => 'image/*',
    'style' => 'width:20%;margin-left:17%;',
    'id' => 'file'
    )) ?>

    <?= $this->Form->end('Create Post') ?>
</div>


<script>
$('.preview_image').hide();

$("#file").change(function () {
    filePreview(this);
});

function filePreview(input)
{
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function (e) {
            $('.preview_image').show();
            $('#prev_img').attr('src', e.target.result);
        };
        reader.readAsDataURL(input.files[0]);
    }
}

$('#remove_image').click(function() {
    $("#file").val(null);
    $('.preview_image').hide();
});
</script>