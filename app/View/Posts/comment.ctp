<div class="nav_bar">
    <div class="home_mypost mt-2">
        <p>
            <?= $this->Html->link("Home", array(
            'controller' => 'posts',
            'action' => 'index'
            )) ?>
        </p>
        <?php if ($this->Session->read('Auth.User.id')) { ?>
        <p>
            <?= $this->Html->link('My Post',array(
            'controller' => 'posts',
            'action' => 'view',
            $this->Session->read('Auth.User.id')
            )) ?></p>
        <?php } ?>
    </div>

    <div class="search_profile">
        <div class="row">
            <div class="col-6 mt-2">
            <?= $this->Form->create('Search', array(
                'url' => array(
                    'controller' => 'searches',
                    'action' => 'search'
                ))) ?>
            <?= $this->Form->input('keywords', array(
                'class' => 'form-control',
                'label' => false,
                'placeholder' => 'Search',
                'style' => 'height: 30px;'
                )) ?>
            <?= $this->Form->end() ?>
            </div>
            <div class="col-6 mt-2">
                <p class="profile_username">
                    <?= $this->Html->link(
                        $this->Session->read('Auth.User.username'), array(
                            'controller' => 'users',
                            'action' => 'view',
                            $this->Session->read('Auth.User.id')
                        )) ?>
                    <i class='fas fa-user'></i>
                </p>
                <p>
                    <?= $this->Html->link(
                    'Logout', array(
                        'controller' => 'users',
                        'action' => 'logout'
                    )) ?>
                    <i class="fas fa-sign-out-alt"></i>
                </p>
            </div>
        </div>
    </div>
</div>

<div class="create_post_container">
<div class="post_content">
    <div class="post_cont">
    <div class="posted_by">
        <?= $this->Html->image('uploads/' . $posts['User']['profile_picture'],
        array(
        'alt' => 'CakePHP',
        'height' => 50,
        'width' => 50,
        'style' => 
        'border-radius: 10%;float:left;margin-right:10px;border:1px solid black;'
        )) ?>
        
        <b>
            <?= $this->Html->link(
            $posts['User']['first_name'], array(
                'controller' => 'users',
                'action' => 'view',
                $posts['User']['id']
            ),array(
                'style' => 'color:#003d4c;'
            )) ?> 
            <?= $this->Html->link(
            $posts['User']['last_name'], array(
                'controller' => 'users',
                'action' => 'view',
                $posts['User']['id']
            ),array(
                'style' => 'color:#003d4c;'
            )) ?>
        </b> &#8226;
        
        <u class="text-muted">
            <?php
                if (strtotime('-365 day') > strtotime($posts['Post']['modified'])) {
                    echo $this->Time->timeAgoInWords($posts['Post']['modified'],
                    array(
                    'accuracy' => array('year' => 'year'),
                    'end' => '1 year'));
                } elseif (strtotime('-30 day') > strtotime($posts['Post']['modified'])) {
                    echo $this->Time->timeAgoInWords($posts['Post']['modified'],
                    array(
                    'accuracy' => array('month' => 'month'),
                    'end' => '1 year'));
                } elseif (strtotime('-7 day') > strtotime($posts['Post']['modified'])) {
                    echo $this->Time->timeAgoInWords($posts['Post']['modified'],
                    array(
                    'accuracy' => array('week' => 'week'),
                    'end' => '1 year'));
                } elseif (strtotime('-1 day') > strtotime($posts['Post']['modified'])) {
                    echo $this->Time->timeAgoInWords($posts['Post']['modified'],
                    array(
                    'accuracy' => array('day' => 'day'),
                    'end' => '1 year'));
                } elseif (strtotime('-1 hour') > strtotime($posts['Post']['modified'])) {
                    echo $this->Time->timeAgoInWords($posts['Post']['modified'],
                    array(
                    'accuracy' => array('hour' => 'hour'),
                    'end' => '1 year'));
                } else {
                    echo $this->Time->timeAgoInWords($posts['Post']['modified'],
                    array(
                    'end' => '1 year'));
                }
            ?>
        </u>
        <?= $this->Html->link(
            $this->Html->tag('i', ' ', array(
            'class' => 'far fa-times-circle')),
            Controller::referer(), array(
            'escape' => false,
            'class' => 'text-muted float-right h5'
            )) ?>
        <br>
        
        <i>@<?= $posts['User']['username'] ?></i>
    </div>
    <div class="post">
        <p class="text-left">
            <?= $posts['Post']['body'] ?>
            <br>
            <?php 
            if ($posts['Post']['image_src']) {
                echo $this->Html->image(
                    'uploads/' . $posts['Post']['image_src'], array(
                    'alt' => 'Image Post',
                    'height' => 300,
                    'style' => 'border-radius:10px;'
                    ));
                }
            ?>
        </p>
        <br>
        <?php
if ($posts['Post']['is_a_share'] == 1) {
        if ($posts['Post']['shared_id'] == $shares['Share']['id']) { ?>
        <div class="shared_cont">
        <div class="posted_by">
            <?= $this->Html->image('uploads/' . 
            $shares['original_user_post']['User']['profile_picture'],
            array(
                'alt' => 'CakePHP',
                'height' => 50,
                'width' => 50,
                'style' => 'border-radius: 10%;float:left;
                    margin-right:10px;border:1px solid black;'
                )) ?>
            <b>
                <?= $this->Html->link(
                $shares['original_user_post']['User']['full_name'],
                array(
                    'controller' => 'users',
                    'action' => 'view',
                    $shares['original_user_post']['User']['id']
                ), array(
                    'style' => 'color:#003d4c;'
                )) ?>
            </b> &#8226;
            
            <u class="text-muted">
                <?php
                if (strtotime('-365 day') > strtotime($shares['Post']['modified'])) {
                    echo $this->Time->timeAgoInWords($shares['Post']['modified'],
                    array(
                    'accuracy' => array('year' => 'year'),
                    'end' => '1 year'));
                } elseif (strtotime('-30 day') > strtotime($shares['Post']['modified'])) {
                    echo $this->Time->timeAgoInWords($shares['Post']['modified'],
                    array(
                    'accuracy' => array('month' => 'month'),
                    'end' => '1 year'));
                } elseif (strtotime('-7 day') > strtotime($shares['Post']['modified'])) {
                    echo $this->Time->timeAgoInWords($shares['Post']['modified'],
                    array(
                    'accuracy' => array('week' => 'week'),
                    'end' => '1 year'));
                } elseif (strtotime('-1 day') > strtotime($shares['Post']['modified'])) {
                    echo $this->Time->timeAgoInWords($shares['Post']['modified'],
                    array(
                    'accuracy' => array('day' => 'day'),
                    'end' => '1 year'));
                } elseif (strtotime('-1 hour') > strtotime($shares['Post']['modified'])) {
                    echo $this->Time->timeAgoInWords($shares['Post']['modified'],
                    array(
                    'accuracy' => array('hour' => 'hour'),
                    'end' => '1 year'));
                } else {
                    echo $this->Time->timeAgoInWords($shares['Post']['modified'],
                    array(
                    'end' => '1 year'));
                }
                ?>
            </u>
            <br>
            
            <i>@<?= $shares['original_user_post']['User']['username'] ?></i>
        </div>
        <div class="post">
        <p class="text-left">
            <?= $shares['Post']['body'] ?>
            <br>
            <?php 
                if ($shares['Post']['image_src']) {
                    echo $this->Html->image('uploads/' . $shares['Post']['image_src'], array(
                        'alt' => 'Image Post',
                        'height' => 300,
                        'style' => 'border-radius:10px;'
                    ));
                }
            ?>
        </p>
        </div>
        </div>
        <?php }} ?>
    </div>
    
    <hr style="height: 12px;border: 0;
    box-shadow: inset 0 12px 12px -12px rgba(0, 0, 0, 0.5);">
    
    <div style="height:100%;width:100%;">
        <p class="text-muted">Comments</p>
        
        <div>
            <?php if ($comments) {?>
            <?php foreach ($comments as $comment): ?>
            <div class="comment_cont">
            <div class="posted_by">
                <?= $this->Html->image(
                'uploads/' . $comment['User']['profile_picture'],
                array(
                'alt' => 'CakePHP',
                'height' => 50,
                'width' => 50,
                'style' => 'border-radius: 30%;float:left;
                    margin-right:10px;border:1px solid black;'
                )) ?>
               
                <b>
                    <?= $this->Html->link(
                    $comment['User']['first_name'], array(
                        'controller' => 'users',
                        'action' => 'view',
                        $comment['User']['id']
                    ),array(
                        'style' => 'color:#003d4c;'
                    )) ?> 
                    <?= $this->Html->link(
                    $comment['User']['last_name'], array(
                        'controller' => 'users',
                        'action' => 'view',
                        $comment['User']['id']
                    ),array(
                        'style' => 'color:#003d4c;'
                    )) ?>
                </b> &#8226;
            
                <u class="text-muted">
                <?php
                if (strtotime('-365 day') > strtotime($comment['Comment']['modified'])) {
                    echo $this->Time->timeAgoInWords($comment['Comment']['modified'],
                    array(
                    'accuracy' => array('year' => 'year'),
                    'end' => '1 year'));
                } elseif (strtotime('-30 day') > strtotime($comment['Comment']['modified'])) {
                    echo $this->Time->timeAgoInWords($comment['Comment']['modified'],
                    array(
                    'accuracy' => array('month' => 'month'),
                    'end' => '1 year'));
                } elseif (strtotime('-7 day') > strtotime($comment['Comment']['modified'])) {
                    echo $this->Time->timeAgoInWords($comment['Comment']['modified'],
                    array(
                    'accuracy' => array('week' => 'week'),
                    'end' => '1 year'));
                } elseif (strtotime('-1 day') > strtotime($comment['Comment']['modified'])) {
                    echo $this->Time->timeAgoInWords($comment['Comment']['modified'],
                    array(
                    'accuracy' => array('day' => 'day'),
                    'end' => '1 year'));
                } elseif (strtotime('-1 hour') > strtotime($comment['Comment']['modified'])) {
                    echo $this->Time->timeAgoInWords($comment['Comment']['modified'],
                    array(
                    'accuracy' => array('hour' => 'hour'),
                    'end' => '1 year'));
                } else {
                    echo $this->Time->timeAgoInWords($comment['Comment']['modified'],
                    array(
                    'end' => '1 year'));
                }
                ?>
                </u>
                <br>
                <p>
                    <?= $comment['Comment']['comment'] ?>
                </p>
                <hr>
            </div>
            </div>
            <?php endforeach; ?>
            <?php unset($comment); } else { ?>
        
        <div class="comment_cont">
            <i>No comments yet. Be the first to write a comment.</i>
        </div>
        <?php }?>
        </div>
            
            <?= $this->Form->create('Comment') ?>
            <div class="row" style="width:100%;margin:auto;">
            <div class="col-7">
                <?= $this->Form->input(
                'comment', array(
                'label' => false,
                'class' => 'form-control',
                'type' => 'text',
                'placeholder' => 'Enter comment here'
                )) ?>
            </div>

            <div class="col-0">
                <?= $this->Form->button(
                'Comment', array(
                'label' => false,
                'class' => 'btn btn-primary'
                )) ?>
            </div>
            <?= $this->Form->end() ?>
        </div>
    </div>
    </div>
</div>
</div>

<script>
$('.preview_image').hide();

$("#file").change(function () {
    $('#image_src').hide();
    filePreview(this);
});

function filePreview(input)
{
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function (e) {
            $('.preview_image').show();
            $('#prev_img').attr('src', e.target.result);
        };
        reader.readAsDataURL(input.files[0]);
    }
}

$('#remove_image').click(function() {
    $("#file").val(null);
    $('.preview_image').hide();
});
</script>