<div class="nav_bar">
    <div class="home_mypost mt-2">
        <p>
            <?= $this->Html->link("Home", array(
            'controller' => 'posts',
            'action' => 'index'
            )) ?>
        </p>
        <?php if ($this->Session->read('Auth.User.id')) { ?>
        <p>
            <?= $this->Html->link('My Post',array(
            'controller' => 'posts',
            'action' => 'view',
            $this->Session->read('Auth.User.id')
            ), array(
            'style' => 'text-decoration: underline;'
            )) ?>
        </p>
        <?php } ?>
    </div>

    <div class="search_profile">
        <div class="row">
            <div class="col-6 mt-2">
            <?= $this->Form->create('Search', array(
                'url' => array(
                    'controller' => 'searches',
                    'action' => 'search'
                ))) ?>
            <?= $this->Form->input('keywords', array(
                'class' => 'form-control',
                'label' => false,
                'placeholder' => 'Search',
                'style' => 'height: 30px;',
                'required' => true
                )) ?>
            <?= $this->Form->end() ?>
            </div>
            <div class="col-6 mt-2">
                <p class="profile_username">
                    <?= $this->Html->link(
                        $this->Session->read('Auth.User.username'), array(
                            'controller' => 'users',
                            'action' => 'view',
                            $this->Session->read('Auth.User.id')
                        )) ?>
                    <i class='fas fa-user'></i>
                </p>
                <p>
                    <?= $this->Html->link(
                    'Logout', array(
                        'controller' => 'users',
                        'action' => 'logout'
                    )) ?>
                    <i class="fas fa-sign-out-alt"></i>
                </p>
            </div>
        </div>
    </div>
</div>

<div class="post_container">
    <div class="post_content">
    <?php if ($posts) { ?>
    <?php foreach ($posts as $post): ?>
    <div class="post_cont">
    <div class="posted_by">
        <?php if ($post['User']['profile_picture'] == 'default.png') { ?>
        <?= $this->Html->image($post['User']['profile_picture'],
        array(
            'alt' => 'CakePHP',
            'height' => 50,
            'width' => 50,
            'style' => 'border-radius: 10%;float:left;margin-right:10px;border:1px solid black;'
            )) ?>
        <?php } else { ?>
        <?= $this->Html->image('uploads/' . $post['User']['profile_picture'],
        array(
            'alt' => 'CakePHP',
            'height' => 50,
            'width' => 50,
            'style' => 'border-radius: 10%;float:left;margin-right:10px;border:1px solid black;'
            )) ?>
        <?php } ?>
        <b>
            <?= $post['User']['first_name'] ?> 
            <?= $post['User']['last_name'] ?>
        </b> &#8226;
        
        <u class="text-muted">
        <?php
                if (strtotime('-365 day') > strtotime($post['Post']['modified'])) {
                    echo $this->Time->timeAgoInWords($post['Post']['modified'],
                    array(
                    'accuracy' => array('year' => 'year'),
                    'end' => '1 year'));
                } elseif (strtotime('-30 day') > strtotime($post['Post']['modified'])) {
                    echo $this->Time->timeAgoInWords($post['Post']['modified'],
                    array(
                    'accuracy' => array('month' => 'month'),
                    'end' => '1 year'));
                } elseif (strtotime('-7 day') > strtotime($post['Post']['modified'])) {
                    echo $this->Time->timeAgoInWords($post['Post']['modified'],
                    array(
                    'accuracy' => array('week' => 'week'),
                    'end' => '1 year'));
                } elseif (strtotime('-1 day') > strtotime($post['Post']['modified'])) {
                    echo $this->Time->timeAgoInWords($post['Post']['modified'],
                    array(
                    'accuracy' => array('day' => 'day'),
                    'end' => '1 year'));
                } elseif (strtotime('-1 hour') > strtotime($post['Post']['modified'])) {
                    echo $this->Time->timeAgoInWords($post['Post']['modified'],
                    array(
                    'accuracy' => array('hour' => 'hour'),
                    'end' => '1 year'));
                } else {
                    echo $this->Time->timeAgoInWords($post['Post']['modified'],
                    array(
                    'end' => '1 year'));
                }
            ?>
        </u>

        <p class="link_actions">
             <?= $this->Form->postLink(
                 $this->Html->tag('i', '', array(
            'class' => 'fas fa-trash')), array(
            'controller' => 'posts',
            'action' => 'delete',
            $post['Post']['id']
            ), array(
            'escape' => false,
            'style' => 'float:right;color:red;',
            'confirm' => 'Are you sure?'
            )) ?>
        </p>

        <p class="link_actions">
             <?= $this->Html->link(
                 $this->Html->tag('i', '', array(
            'class' => 'fas fa-edit')), array(
            'controller' => 'posts',
            'action' => 'edit',
            $post['Post']['id']
            ), array(
            'escape' => false,
            'style' => 'float:right;margin-right:10px;color:blue;'
            )) ?>
        </p>
        <br>
        <i>@<?= $post['User']['username'] ?></i>
    </div>
    <div class="post">
        <p class="text-left">
            <?= $post['Post']['body'] ?>
            <br>
            <?php 
                if ($post['Post']['image_src']) {
                    echo $this->Html->image('uploads/' . $post['Post']['image_src'], array(
                        'alt' => 'Image Post',
                        'height' => 300,
                        'style' => 'border-radius:10px;'
                    ));
                }
            ?>
        </p>
        <br>
        <?php
if ($post['Post']['is_a_share'] == 1) {
    for ($i=0;$i<count($shares);$i++) {
        if ($post['Post']['shared_id'] == $shares[$i]['Share']['id']) { ?>
        <div class="shared_cont">
        <div class="posted_by">
            <?php if ($shares[$i]['original_user_post']['User']['profile_picture']
            == 'default.png') { ?>
            <?= $this->Html->image(
            $shares[$i]['original_user_post']['User']['profile_picture'],
            array(
                'alt' => 'CakePHP',
                'height' => 50,
                'width' => 50,
                'style' => 'border-radius: 10%;float:left;
                    margin-right:10px;border:1px solid black;'
                )) ?>
            <?php } else { ?>
            <?= $this->Html->image('uploads/' . 
            $shares[$i]['original_user_post']['User']['profile_picture'],
            array(
                'alt' => 'CakePHP',
                'height' => 50,
                'width' => 50,
                'style' => 'border-radius: 10%;float:left;
                    margin-right:10px;border:1px solid black;'
                )) ?>
            <?php } ?>
            <b>
                <?= $this->Html->link(
                $shares[$i]['original_user_post']['User']['full_name'],
                array(
                    'controller' => 'users',
                    'action' => 'view',
                    $shares[$i]['original_user_post']['User']['id']
                ), array(
                    'style' => 'color:#003d4c;'
                )) ?>
            </b> &#8226;
            
            <u class="text-muted">
                <?php
                if (strtotime('-365 day') > strtotime($shares[$i]['Post']['modified'])) {
                    echo $this->Time->timeAgoInWords($shares[$i]['Post']['modified'],
                    array(
                    'accuracy' => array('year' => 'year'),
                    'end' => '1 year'));
                } elseif (strtotime('-30 day') > strtotime($shares[$i]['Post']['modified'])) {
                    echo $this->Time->timeAgoInWords($shares[$i]['Post']['modified'],
                    array(
                    'accuracy' => array('month' => 'month'),
                    'end' => '1 year'));
                } elseif (strtotime('-7 day') > strtotime($shares[$i]['Post']['modified'])) {
                    echo $this->Time->timeAgoInWords($shares[$i]['Post']['modified'],
                    array(
                    'accuracy' => array('week' => 'week'),
                    'end' => '1 year'));
                } elseif (strtotime('-1 day') > strtotime($shares[$i]['Post']['modified'])) {
                    echo $this->Time->timeAgoInWords($shares[$i]['Post']['modified'],
                    array(
                    'accuracy' => array('day' => 'day'),
                    'end' => '1 year'));
                } elseif (strtotime('-1 hour') > strtotime($shares[$i]['Post']['modified'])) {
                    echo $this->Time->timeAgoInWords($shares[$i]['Post']['modified'],
                    array(
                    'accuracy' => array('hour' => 'hour'),
                    'end' => '1 year'));
                } else {
                    echo $this->Time->timeAgoInWords($shares[$i]['Post']['modified'],
                    array(
                    'end' => '1 year'));
                }
            ?>
            </u>
            <br>
            
            <i>@<?= $shares[$i]['original_user_post']['User']['username'] ?></i>
        </div>
        <div class="post">
        <p class="text-left">
            <?= $shares[$i]['Post']['body'] ?>
            <br>
            <?php 
                if ($shares[$i]['Post']['image_src']) {
                    echo $this->Html->image('uploads/' . $shares[$i]['Post']['image_src'], array(
                        'alt' => 'Image Post',
                        'height' => 300,
                        'style' => 'border-radius:10px;'
                    ));
                }
            ?>
        </p>
        </div>
        </div>
        <?php }}} ?>
    </div>
    </div>
    <?php endforeach; ?>
    <?php unset($post); ?>
    <?php } else { ?>
    <div class="no-post">
        <h3 class="text-muted font-italic text-center">No posts yet</h3>
    </div>
    <?php } ?>
    <div class="paginator">
        <p>
            <?= $this->Paginator->prev(
            '« Previous',
            null,
            null, array(
            'class' => 'prev d-none'
            )) ?>
        </p>
        
        <p>
            <?= $this->Paginator->next(
            'Next »',
            null,
            null, array(
            'class' => 'next d-none'
            )) ?>
        </p>
    </div>
   </div>
</div>