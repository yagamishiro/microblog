<div class="nav_bar">
    <div class="home_mypost mt-2">
        <p>
            <?= $this->Html->link("Home", array(
            'controller' => 'posts',
            'action' => 'index'
            )) ?>
        </p>
        <?php if ($this->Session->read('Auth.User.id')) { ?>
        <p>
            <?= $this->Html->link('My Post',array(
            'controller' => 'posts',
            'action' => 'view',
            $this->Session->read('Auth.User.id')
            )) ?></p>
        <?php } ?>
    </div>

    <div class="search_profile">
        <div class="row">
            <div class="col-6 mt-2">
            <?= $this->Form->create('Search', array(
                'url' => array(
                    'controller' => 'searches',
                    'action' => 'search'
                ))) ?>
            <?= $this->Form->input('keywords', array(
                'class' => 'form-control',
                'label' => false,
                'placeholder' => 'Search',
                'style' => 'height: 30px;',
                'required' => true
                )) ?>
            <?= $this->Form->end() ?>
            </div>
            <div class="col-6 mt-2">
                <p class="profile_username">
                    <?= $this->Html->link(
                        $this->Session->read('Auth.User.username'), array(
                            'controller' => 'users',
                            'action' => 'view',
                            $this->Session->read('Auth.User.id')
                        )) ?>
                    <i class='fas fa-user'></i>
                </p>
                <p>
                    <?= $this->Html->link(
                    'Logout', array(
                        'controller' => 'users',
                        'action' => 'logout'
                    )) ?>
                    <i class="fas fa-sign-out-alt"></i>
                </p>
            </div>
        </div>
    </div>
</div>

<div class="create_post_container">
    <div class="create_post_header">
        <p class="create_post_title">Write Post</p>
    </div>
    <?= $this->Form->create('Post') ?>

    <?= $this->Form->input('body', array(
        'rows' => '3',
        'label' => false,
        'class' => 'post_body'
    )) ?>

    <div class="post_body border-0">
    <div class="post_cont">
    <div class="posted_by">
        <?php if ($posts['User']['profile_picture'] == 'default.png') { ?>
        <?= $this->Html->image($posts['User']['profile_picture'], array(
            'alt' => 'CakePHP',
            'height' => 50,
            'width' => 50,
            'style' => 'border-radius: 10%;float:left;margin-right:10px;border:1px solid black;'
            )) ?>
        <?php } else { ?>
        <?= $this->Html->image('/uploads'.$posts['User']['profile_picture'], array(
            'alt' => 'CakePHP',
            'height' => 50,
            'width' => 50,
            'style' => 'border-radius: 10%;float:left;margin-right:10px;border:1px solid black;'
            )) ?>
        <?php } ?>
        <b>
            <?= $posts['User']['first_name'] ?> 
            <?= $posts['User']['last_name'] ?>
        </b> &#8226;
        
        <u class="text-muted">
            <?= $this->Time->format($posts['Post']['modified'],
            '%H:%M %p') ?> - <?= $this->Time->format($posts['Post']['modified'],
            '%b %e, %Y') ?>
        </u>
        <br>
        <i>@<?= $posts['User']['username'] ?></i>
    </div>
    <div class="post">
        <p class="text-left">
            <?= $posts['Post']['body'] ?>
            <br>
            <?php 
                if ($posts['Post']['image_src']) {
                    echo $this->Html->image('uploads/' . $posts['Post']['image_src'], array(
                        'alt' => 'Image Post',
                        'height' => 300,
                        'style' => 'border-radius:10px;'
                    ));
                }
            ?>
        </p>
    </div>
    </div>
    <div>    
    <?= $this->Form->end('Share Post') ?>
</div>