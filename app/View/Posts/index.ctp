<div class="nav_bar">
    <div class="home_mypost mt-2">
        <p>
            <?= $this->Html->link("Home", array(
                'controller' => 'posts',
                'action' => 'index'
            ), array(
                'style' => 'text-decoration: underline;'
            )) ?>
        </p>
        <?php if ($this->Session->read('Auth.User.id')) { ?>
        <p>
            <?= $this->Html->link('My Post',array(
            'controller' => 'posts',
            'action' => 'view',
            $this->Session->read('Auth.User.id')
            )) ?></p>
        <?php } ?>
    </div>

    <div class="search_profile">
        <div class="row">
            <div class="col-6 mt-2">
            <?= $this->Form->create('Search', array(
                'url' => array(
                    'controller' => 'searches',
                    'action' => 'search'
                ))) ?>
            <?= $this->Form->input('keywords', array(
                'class' => 'form-control',
                'label' => false,
                'placeholder' => 'Search',
                'style' => 'height: 30px;',
                'required' => true
                )) ?>
            <?= $this->Form->end() ?>
            </div>

            <?php if ($this->Session->read('Auth.User.id')) { ?>
            <div class="col-6 mt-2">
                <p class="profile_username">
                    <?= $this->Html->link(
                        $this->Session->read('Auth.User.username'), array(
                            'controller' => 'users',
                            'action' => 'view',
                            $this->Session->read('Auth.User.id')
                        )) ?>
                    <i class='fas fa-user'></i>
                </p>
                
                <p>
                    <?= $this->Html->link(
                    'Logout', array(
                        'controller' => 'users',
                        'action' => 'logout'
                    )) ?>
                    <i class="fas fa-sign-out-alt"></i>
                </p>
            </div>
            <?php } else { ?>
            <div class="col-6 mt-2">
            <p class="profile_username">
                <?= $this->Html->link(
                    'Login/Register', array(
                        'controller' => 'users',
                        'action' => 'login',
                    )) ?>
                <i class="fas fa-sign-in-alt"></i>
            </p>
            </div>
            <?php } ?>
        </div>
    </div>
</div>

<div class="post_container">
    <div class="create_post">
        <?= $this->Html->link(
        'Create Post', array(
            'posts',
            'action' => 'add'
        ), array(
            'class' => 'btn btn-outline-primary',
            'style' => 'float: right;'
        )) ?>
    </div>

    <div class="post_content">
    <?php if ($posts) { ?>
    <?php foreach ($posts as $post): ?>
    <div class="post_cont">
    <div class="posted_by">
        <?php if ($post['User']['profile_picture'] == 'default.png') { ?>
        <?= $this->Html->image($post['User']['profile_picture'],
        array(
            'alt' => 'CakePHP',
            'height' => 50,
            'width' => 50,
            'style' => 'border-radius: 10%;float:left;margin-right:10px;border:1px solid black;'
            )) ?>
        <?php } else { ?>
        <?= $this->Html->image('uploads/' . $post['User']['profile_picture'],
        array(
            'alt' => 'CakePHP',
            'height' => 50,
            'width' => 50,
            'style' => 'border-radius: 10%;float:left;margin-right:10px;border:1px solid black;'
            )) ?>
        <?php } ?>
        <b>
            <?= $this->Html->link(
            $post['User']['full_name'], array(
                'controller' => 'users',
                'action' => 'view',
                $post['User']['id']
            ),array(
                'style' => 'color:#003d4c;'
            )) ?>
        </b> &#8226;
        
        <u class="text-muted">
            <?php
                if (strtotime('-365 day') > strtotime($post['Post']['modified'])) {
                    echo $this->Time->timeAgoInWords($post['Post']['modified'],
                    array(
                    'accuracy' => array('year' => 'year'),
                    'end' => '1 year'));
                } elseif (strtotime('-30 day') > strtotime($post['Post']['modified'])) {
                    echo $this->Time->timeAgoInWords($post['Post']['modified'],
                    array(
                    'accuracy' => array('month' => 'month'),
                    'end' => '1 year'));
                } elseif (strtotime('-7 day') > strtotime($post['Post']['modified'])) {
                    echo $this->Time->timeAgoInWords($post['Post']['modified'],
                    array(
                    'accuracy' => array('week' => 'week'),
                    'end' => '1 year'));
                } elseif (strtotime('-1 day') > strtotime($post['Post']['modified'])) {
                    echo $this->Time->timeAgoInWords($post['Post']['modified'],
                    array(
                    'accuracy' => array('day' => 'day'),
                    'end' => '1 year'));
                } elseif (strtotime('-1 hour') > strtotime($post['Post']['modified'])) {
                    echo $this->Time->timeAgoInWords($post['Post']['modified'],
                    array(
                    'accuracy' => array('hour' => 'hour'),
                    'end' => '1 year'));
                } else {
                    echo $this->Time->timeAgoInWords($post['Post']['modified'],
                    array(
                    'end' => '1 year'));
                }
            ?>
        </u>
        <?php if ($post['Post']['user_id'] == $this->Session->read('Auth.User.id')) {?>
        <div class="btn-group float-right">
        <button type="button" class="btn flat" data-toggle="dropdown"
        aria-haspopup="true" aria-expanded="false" style="background-color: transparent;">
            <i class="fas fa-ellipsis-h"></i>
        </button>
        <div class="dropdown-menu dropdown-menu-right">
        <?= $this->Html->link(
            $this->Html->tag('i', '  Edit', array(
            'class' => 'fas fa-edit')), array(
            'controller' => 'posts',
            'action' => 'edit',
            $post['Post']['id']
            ), array(
            'escape' => false,
            'class' => 'dropdown-item'
            )) ?>
        <?= $this->Form->postLink(
            $this->Html->tag('i', '  Delete', array(
            'class' => 'fas fa-trash')), array(
            'controller' => 'posts',
            'action' => 'delete',
            $post['Post']['id']
            ), array(
            'escape' => false,
            'class' => 'dropdown-item',
            'confirm' => 'Are you sure?'
            )) ?>
        </div>
        </div>
        <?php } else { ?>
            <div class="btn-group float-right">
        <button type="button" class="btn flat" data-toggle="dropdown"
        aria-haspopup="true" aria-expanded="false" style="background-color: transparent;">
            <i class="fas fa-ellipsis-h"></i>
        </button>
        <div class="dropdown-menu dropdown-menu-right">
        <?= $this->Form->postLink(
            $this->Html->tag('i', '  Unfollow ' . $post['User']['first_name'], array(
            'class' => 'fas fa-user-times')), array(
            'controller' => 'users',
            'action' => 'unfollow',
            $post['User']['id']
            ), array(
            'escape' => false,
            'class' => 'dropdown-item'
            )) ?>
        </div>
        </div>
        <?php } ?>
        <br>
        
        <i>@<?= $post['User']['username'] ?></i>
    </div>
    <div class="post">
        <p class="text-left">
            <?= $post['Post']['body'] ?>
            <br>
            <?php 
                if ($post['Post']['image_src']) {
                    echo $this->Html->image('uploads/' . $post['Post']['image_src'], array(
                        'alt' => 'Image Post',
                        'height' => 300,
                        'style' => 'border-radius:10px;'
                    ));
                }
            ?>
        </p>
        <br>
<?php
if ($post['Post']['is_a_share'] == 1) {
    for ($i=0;$i<count($shares);$i++) {
        if ($post['Post']['shared_id'] == $shares[$i]['Share']['id']) { ?>
        <div class="shared_cont">
        <div class="posted_by">
            <?php if ($shares[$i]['original_user_post']['User']['profile_picture']
            == 'default.png') { ?>
            <?= $this->Html->image(
            $shares[$i]['original_user_post']['User']['profile_picture'],
            array(
                'alt' => 'CakePHP',
                'height' => 50,
                'width' => 50,
                'style' => 'border-radius: 10%;float:left;
                    margin-right:10px;border:1px solid black;'
                )) ?>
            <?php } else { ?>
            <?= $this->Html->image('uploads/' . 
            $shares[$i]['original_user_post']['User']['profile_picture'],
            array(
                'alt' => 'CakePHP',
                'height' => 50,
                'width' => 50,
                'style' => 'border-radius: 10%;float:left;
                    margin-right:10px;border:1px solid black;'
                )) ?>
            <?php } ?>
            <b>
                <?= $this->Html->link(
                $shares[$i]['original_user_post']['User']['full_name'],
                array(
                    'controller' => 'users',
                    'action' => 'view',
                    $shares[$i]['original_user_post']['User']['id']
                ), array(
                    'style' => 'color:#003d4c;'
                )) ?>
            </b> &#8226;
            
            <u class="text-muted">
                <?php
                if (strtotime('-365 day') > strtotime($shares[$i]['Post']['modified'])) {
                    echo $this->Time->timeAgoInWords($shares[$i]['Post']['modified'],
                    array(
                    'accuracy' => array('year' => 'year'),
                    'end' => '1 year'));
                } elseif (strtotime('-30 day') > strtotime($shares[$i]['Post']['modified'])) {
                    echo $this->Time->timeAgoInWords($shares[$i]['Post']['modified'],
                    array(
                    'accuracy' => array('month' => 'month'),
                    'end' => '1 year'));
                } elseif (strtotime('-7 day') > strtotime($shares[$i]['Post']['modified'])) {
                    echo $this->Time->timeAgoInWords($shares[$i]['Post']['modified'],
                    array(
                    'accuracy' => array('week' => 'week'),
                    'end' => '1 year'));
                } elseif (strtotime('-1 day') > strtotime($shares[$i]['Post']['modified'])) {
                    echo $this->Time->timeAgoInWords($shares[$i]['Post']['modified'],
                    array(
                    'accuracy' => array('day' => 'day'),
                    'end' => '1 year'));
                } elseif (strtotime('-1 hour') > strtotime($shares[$i]['Post']['modified'])) {
                    echo $this->Time->timeAgoInWords($shares[$i]['Post']['modified'],
                    array(
                    'accuracy' => array('hour' => 'hour'),
                    'end' => '1 year'));
                } else {
                    echo $this->Time->timeAgoInWords($shares[$i]['Post']['modified'],
                    array(
                    'end' => '1 year'));
                }
            ?>
            </u>
            <br>
            
            <i>@<?= $shares[$i]['original_user_post']['User']['username'] ?></i>
        </div>
        <div class="post">
        <p class="text-left">
            <?= $shares[$i]['Post']['body'] ?>
            <br>
            <?php 
                if ($shares[$i]['Post']['image_src']) {
                    echo $this->Html->image('uploads/' . $shares[$i]['Post']['image_src'], array(
                        'alt' => 'Image Post',
                        'height' => 300,
                        'style' => 'border-radius:10px;'
                    ));
                }
            ?>
        </p>
        </div>
        </div>
        <?php }}} ?>
    </div>
    <div class="lcs_actions">
        <!-- Likes Section -->
        <span>
            <?php
            if ($post['extra']['like_by_me']) {
                echo $this->Form->button(
                    $this->Html->tag('i','', array(
                        'class' => 'fas fa-heart')),
                    array(
                        'type' => 'button',
                        'class' => 'btn btn-md actionsBtn flat',
                        'id' => 'nl'.$post['Post']['id'],
                        'style' => 'background-color: transparent;',
                        'escape' => false
                    ));
            } else {
                echo $this->Form->button(
                    $this->Html->tag('i','', array(
                        'class' => 'far fa-heart')),
                    array(
                        'type' => 'button',
                        'class' => 'btn btn-md actionsBtn flat likeBtn',
                        'id' => 'nl'.$post['Post']['id'],
                        'style' => 'background-color: transparent;',
                        'onclick' => "liked(".$post['Post']['id'].")",
                        'escape' => false
                    ));
            } ?>
            <?= $this->Form->button(
                    $this->Html->tag('i','',
                    array(
                        'class' => 'fas fa-heart')),
                array(
                    'type' => 'button',
                    'class' => 'btn btn-md actionsBtn flat',
                    'id' => 'l'.$post['Post']['id'],
                    'style' => 'background-color: transparent;display: none;',
                    'escape' => false
                )); ?>

            <p class="like_count<?=$post['Post']['id']?>" 
            style="padding:0 !important;font-size:15px;">
                <?= $post['Post']['like_count'] ? $post['Post']['like_count'] : '' ?>
            </p>
        </span>

        <!-- Comment Section -->
        <span>
            <?=
            $this->Form->button(
                $this->Html->tag('i','', array(
                    'class' => 'fas fa-comments-alt')),
                array(
                    'type' => 'button',
                    'class' => 'btn btn-md flat actionsBtn commentBtn',
                    'style' => 'background-color: transparent;',
                    'onclick' => 'comment('.$post['Post']['id'].')',
                    'escape' => false,
                ));
            ?>

            <p style="padding:0;font-size:15px;">
                <?= $post['Post']['comment_count'] ? $post['Post']['comment_count'] : '' ?>
            </p>
        </span>

        <!-- Share Section -->
        <span>
        <?php if ($post['Post']['is_a_share'] == 1) { ?>
            <?= $this->Html->link(
                $this->Html->tag('i','', array(
                    'class' => 'fas fa-retweet')), array(
                        'controller' => 'posts',
                        'action' => 'share',
                        $post['share_info']['id']
                        ), 
                array(
                    'escape' => false,
                    'class' => 'btn btn-md'
                ));
            ?>
        <?php } else { ?>
            <?= $this->Html->link(
                $this->Html->tag('i','', array(
                    'class' => 'fas fa-retweet')), array(
                        'controller' => 'posts',
                        'action' => 'share',
                        $post['Post']['id']
                        ), 
                array(
                    'escape' => false,
                    'class' => 'btn btn-md'
                ));
            ?>
        <?php } ?>
            
        <p style="padding:0;font-size:15px;">
            <?= $post['Post']['share_count'] ? $post['Post']['share_count'] : '' ?>
        </p>
        </span>
        <div class="comments<?=$post['Post']['id']?>" style="display:none;"></div>
        <?= $this->Form->create('Comment', array(
            'class' => 'addComment'.$post['Post']['id'],
            'style' => 'display: none;'
        )) ?>
        <div class="row" style="width:100%;margin:auto;">
            <div class="col-7">
                <?= $this->Form->input(
                'comment', array(
                'label' => false,
                'class' => 'form-control commentBody'.$post['Post']['id'],
                'type' => 'text',
                'placeholder' => 'Enter comment here'
                )) ?>
            </div>

            <div class="col-0">
                <?= $this->Form->button(
                'Comment', array(
                'label' => false,
                'class' => 'btn btn-primary addCommentBtn',
                'onclick' => 'addComment('.$post['Post']['id'].')',
                'type' => 'button'
                )) ?>
            </div>
        </div>
    <?= $this->Form->end() ?>
    </div>
    </div>
    <?php endforeach; ?>
    <?php unset($post); ?>
    <?php } else { ?>
    <h3 class="text-muted font-italic text-center">No posts yet</h3>
    <?php } ?>
    <!-- End of post display -->
    
    <div class="paginator">
        <p>
            <?= $this->Paginator->prev(
            '« Previous',
            null,
            null, array(
            'class' => 'prev d-none'
            )) ?>
        </p>
        
        <p>
            <?= $this->Paginator->next(
            'Next »',
            null,
            null, array(
            'class' => 'next d-none'
            )) ?>
        </p>
    </div>
   </div>
</div>

<!-- AJAX script -->
<script>
function liked(id)
{
    $(document).ready(function() {
    $.ajax({
    type: 'POST',
    url: '<?php echo Router::url(
        array(
        'controller' => 'likes',
        'action' => 'likes'),
        true).'/'; ?>'+id,
    data: {id: id},
    dataType: 'HTML',
    headers: {'X-Requested-With': 'XMLHttpRequest'},
    success: function(response) {
        $('#nl'+id).hide();
        $('.like_count'+id).html(response);
        $('#l'+id).show();
    }
    });
    });
}
function comment(id)
{
    $(document).ready(function() {
    $.ajax({
    type: 'GET',
    evalScripts: true,
    url: '<?php echo Router::url(
        array(
        'controller' => 'posts',
        'action' => 'comment'),
        true).'/'; ?>'+id,
    dataType: 'HTML',
    headers: {'X-Requested-With': 'XMLHttpRequest'},
    success: function(response) {
        $('.comments'+id).show();
        $('.addComment'+id).show();
        $('.comments'+id).html(response);
    }
    });
    });
}

function addComment(id)
{
let commentBody = $('.commentBody'+id).val();
$(document).ready(function() {
    $.ajax({
    type: 'POST',
    url: '<?php echo Router::url(
        array(
        'controller' => 'posts',
        'action' => 'addComment'),
        true).'/'; ?>'+id,
    data: {comment: commentBody},
    dataType: 'HTML',
    headers: {'X-Requested-With': 'XMLHttpRequest'},
    success: function(response) {
        $('.comments'+id).show();
        $('.comments'+id).html(response);
    }
    });
});
}
</script>