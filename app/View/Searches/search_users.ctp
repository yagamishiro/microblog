<div class="nav_bar">
    <div class="home_mypost mt-2">
        <p>
            <?= $this->Html->link("Home",
            array(
            'controller' => 'posts',
            'action' => 'index'
            )) ?>
        </p>
        <?php if ($this->Session->read('Auth.User.id')) { ?>
        <p>
            <?= $this->Html->link('My Post',array(
            'controller' => 'posts',
            'action' => 'view',
            $this->Session->read('Auth.User.id')
            )) ?></p>
        <?php } ?>
    </div>

    <div class="search_profile">
        <div class="row">
            <div class="col-6 mt-2">
            <?= $this->Form->create('Search', array(
                'url' => array(
                    'controller' => 'searches',
                    'action' => 'search'
                ))) ?>
            <?= $this->Form->input('keywords', array(
                'class' => 'form-control',
                'label' => false,
                'placeholder' => 'Search',
                'style' => 'height: 30px;',
                'required' => true
                )) ?>
            <?= $this->Form->end() ?>
            </div>
            <?php if ($this->Session->read('Auth.User.id')) { ?>
            <div class="col-6 mt-2">
                <p class="profile_username">
                    <?= $this->Html->link(
                        $this->Session->read('Auth.User.username'), array(
                            'controller' => 'users',
                            'action' => 'view',
                            $this->Session->read('Auth.User.id')
                        )) ?>
                    <i class='fas fa-user'></i>
                </p>
                
                <p>
                    <?= $this->Html->link(
                    'Logout', array(
                        'controller' => 'users',
                        'action' => 'logout'
                    )) ?>
                    <i class="fas fa-sign-out-alt"></i>
                </p>
            </div>
            <?php } else { ?>
            <div class="col-6 mt-2">
            <p class="profile_username">
                <?= $this->Html->link(
                    'Login/Register', array(
                        'controller' => 'users',
                        'action' => 'login',
                    )) ?>
                <i class="fas fa-sign-in-alt"></i>
            </p>
            </div>
            <?php } ?>
        </div>
    </div>
</div>


<div class="post_container">
    <div class="create_post">
        <h3>Search Results for:  <?= $search_keyword ?></h3>
            <p>
                <?= $this->Form->postLink(
                'All', array(
                'controller' => 'searches',
                'action' => 'search',
                $search_keyword
                ), array(
                'class' => 'btn btn-outline-primary'
                )) ?>
            
                <?= $this->Form->postLink(
                'Users', array(
                'controller' => 'searches',
                'action' => 'search_users',
                $search_keyword
                ), array(
                'class' => 'btn btn-primary'
                )) ?>
            
                <?= $this->Form->postLink(
                'Posts', array(
                'controller' => 'searches',
                'action' => 'search_posts',
                $search_keyword
                ), array(
                'class' => 'btn btn-outline-primary'
                )) ?>
            </p>
    </div>

    <div class="post_content">
    <?php foreach ($user_searches as $user_search):?>
        <div class="post_cont">
        <div class="posted_by p-2">
        <?php if ($user_search['User']['profile_picture'] == 'default.png') { ?>
        <?= $this->Html->image(
            $user_search['User']['profile_picture'], 
            array(
            'alt' => 'CakePHP',
            'height' => 50,
            'width' => 50,
            'style' => 'border-radius: 10%;float:left;margin-right:10px;border:1px solid black;'
            )) ?>
        <?php } else { ?>
        <?= $this->Html->image(
            'uploads/' . $user_search['User']['profile_picture'], 
            array(
            'alt' => 'CakePHP',
            'height' => 50,
            'width' => 50,
            'style' => 'border-radius: 10%;float:left;margin-right:10px;border:1px solid black;'
            )) ?>
        <?php } ?>
        <b>
            <?= $this->Html->link(
            $user_search['User']['first_name'], array(
                'controller' => 'users',
                'action' => 'view',
                $user_search['User']['id']
            ), array(
                'style' => 'color:#003d4c;'
            )) ?>
        </b>
            
        <b>
            <?= $this->Html->link(
            $user_search['User']['last_name'], array(
                'controller' => 'users',
                'action' => 'view',
                $user_search['User']['id']
            ), array(
                'style' => 'color:#003d4c;'
            )) ?>
        </b>
        <br>
        <i>@<?= $user_search['User']['username'] ?></i>
        </div>
        </div>
    <?php endforeach;?>
    <?php unset($user_search); ?>
    <div class="paginator">
        <p>
            <?= $this->Paginator->prev(
            '« Previous',
            null,
            null, array(
            'class' => 'prev d-none'
            )) ?>
        </p>
        
        <p>
            <?= $this->Paginator->next(
            'Next »',
            null,
            null, array(
            'class' => 'next d-none'
            )) ?>
        </p>
    </div>
   </div>
</div>