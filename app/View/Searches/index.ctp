<div class="nav_bar">
    <div class="home_mypost mt-2">
        <p>
            <?= $this->Html->link("Home", array(
            'controller' => 'posts',
            'action' => 'index'
            )) ?>
        </p>
        <?php if ($this->Session->read('Auth.User.id')) { ?>
        <p>
            <?= $this->Html->link('My Post',array(
            'controller' => 'posts',
            'action' => 'view',
            $this->Session->read('Auth.User.id')
            )) ?></p>
        <?php } ?>
    </div>

    <div class="search_profile">
        <div class="row">
            <div class="col-6 mt-2">
                <?= $this->Form->create('Search', array(
                    'url' => array(
                        'controller' => 'searches',
                        'action' => 'search'
                    )));
                    $this->Form->input('keywords', array(
                        'class' => 'form-control',
                        'label' => false,
                        'placeholder' => 'Search',
                        'style' => 'height: 30px;',
                        'required' => true
                    ));
                    $this->Form->end()
                ?>
            </div>
            <?php if ($this->Session->read('Auth.User.id')) { ?>
            <div class="col-6 mt-2">
                <p class="profile_username">
                    <?= $this->Html->link(
                        $this->Session->read('Auth.User.username'), array(
                            'controller' => 'users',
                            'action' => 'view',
                            $this->Session->read('Auth.User.id')
                        )) ?>
                    <i class='fas fa-user'></i>
                </p>
                <p>
                    <?= $this->Html->link(
                    'Logout', array(
                        'controller' => 'users',
                        'action' => 'logout'
                    )) ?>
                    <i class="fas fa-sign-out-alt"></i>
                </p>
            </div>
            <?php } else { ?>
            <div class="col-6 mt-2">
            <p class="profile_username">
                <?= $this->Html->link(
                    'Login/Register', array(
                        'controller' => 'users',
                        'action' => 'login',
                    )) ?>
                <i class="fas fa-sign-in-alt"></i>
            </p>
            </div>
            <?php } ?>
        </div>
    </div>
</div>

<div class="post_container">
    <div class="create_post">
        <h3>Search Results for:  <?= $search_keyword ?></h3>
        <?php if (empty($user_searches) && empty($post_searches)) { ?>
        <h2 class="text-center mt-5">Nothing Found</h2>
        <h4 class="text-muted">Sorry, but nothing matched your search terms. Please try
        again with some different keywords.</h4>
        <?php } else { ?>
            <p>
                <?= $this->Form->postLink(
                'All', array(
                'controller' => 'searches',
                'action' => 'search',
                $search_keyword
                ), array(
                'class' => 'btn btn-primary'
                )) ?>
                
                <?= $this->Form->postLink(
                'Users', array(
                'controller' => 'searches',
                'action' => 'search_users',
                $search_keyword
                ), array(
                'class' => 'btn btn-outline-primary'
                )) ?>
            
                <?= $this->Form->postLink(
                'Posts', array(
                'controller' => 'searches',
                'action' => 'search_posts',
                $search_keyword
                ), array(
                'class' => 'btn btn-outline-primary'
                )) ?>
            </p>
    </div>

    <div class="post_content">
    <?php foreach ($user_searches as $user_search):?>
        <div class="post_cont">
        <div class="posted_by">
        <?php if ($user_search['User']['profile_picture'] == 'default.png') { ?>
        <?= $this->Html->image(
            $user_search['User']['profile_picture'], 
            array(
            'alt' => 'CakePHP',
            'height' => 50,
            'width' => 50,
            'style' => 'border-radius: 10%;float:left;margin-right:10px;border:1px solid black;'
            )) ?>
        <?php } else { ?>
        <?= $this->Html->image(
            'uploads/' . $user_search['User']['profile_picture'], 
            array(
            'alt' => 'CakePHP',
            'height' => 50,
            'width' => 50,
            'style' => 'border-radius: 10%;float:left;margin-right:10px;border:1px solid black;'
            )) ?>
        <?php } ?>
        <b>
            <?= $this->Html->link(
            $user_search['User']['full_name'], array(
                'controller' => 'users',
                'action' => 'view',
                $user_search['User']['id']
            ), array(
                'style' => 'color:#003d4c;'
            )) ?>
        </b>
        <br>
        
        <i class="text-muted">@<?= $user_search['User']['username'] ?></i>
        </div>
        
        <?= $user_search['Count']['followers'] ?> Followers
        </div>
    <?php endforeach;?>
    <?php unset($user_search); ?>
    <p>
        <?php if (count($user_searches) > 5) {?>
        <?= $this->Form->postLink(
        'See more...', array(
            'controller' => 'searches',
            'action' => 'search_users',
            $search_keyword
        ), array(
            'style' => 'color:#003d4c;float:right'
        )) ?>
    </p>
    <?php } ?>
   </div>
   <div class="post_content posts_div">
    <?php foreach ($post_searches as $key => $post_search):?>
        <div class="post_cont">
        <div class="posted_by">
        <?php if ($post_search['User']['profile_picture'] == 'default.png') { ?>
        <?= $this->Html->image(
            $post_search['User']['profile_picture'], 
            array(
            'alt' => 'CakePHP',
            'height' => 50,
            'width' => 50,
            'style' => 'border-radius: 10%;float:left;margin-right:10px;border:1px solid black;'
            )) ?>
        <?php } else { ?>
        <?= $this->Html->image(
            'uploads/' . $post_search['User']['profile_picture'], 
            array(
            'alt' => 'CakePHP',
            'height' => 50,
            'width' => 50,
            'style' => 'border-radius: 10%;float:left;margin-right:10px;border:1px solid black;'
            )) ?>
        <?php } ?>
        <b>
            <?= $this->Html->link(
            $post_search['User']['full_name'], array(
                'controller' => 'users',
                'action' => 'view',
                $post_search['User']['id']
            ), array(
                'style' => 'color:#003d4c;'
            )) ?>
        </b> &#8226;
        
        <u class="text-muted">
        <?php
                if (strtotime('-365 day') > strtotime($post_search['Post']['modified'])) {
                    echo $this->Time->timeAgoInWords($post_search['Post']['modified'],
                    array(
                    'accuracy' => array('year' => 'year'),
                    'end' => '1 year'));
                } elseif (strtotime('-30 day') > strtotime($post_search['Post']['modified'])) {
                    echo $this->Time->timeAgoInWords($post_search['Post']['modified'],
                    array(
                    'accuracy' => array('month' => 'month'),
                    'end' => '1 year'));
                } elseif (strtotime('-7 day') > strtotime($post_search['Post']['modified'])) {
                    echo $this->Time->timeAgoInWords($post_search['Post']['modified'],
                    array(
                    'accuracy' => array('week' => 'week'),
                    'end' => '1 year'));
                } elseif (strtotime('-1 day') > strtotime($post_search['Post']['modified'])) {
                    echo $this->Time->timeAgoInWords($post_search['Post']['modified'],
                    array(
                    'accuracy' => array('day' => 'day'),
                    'end' => '1 year'));
                } elseif (strtotime('-1 hour') > strtotime($post_search['Post']['modified'])) {
                    echo $this->Time->timeAgoInWords($post_search['Post']['modified'],
                    array(
                    'accuracy' => array('hour' => 'hour'),
                    'end' => '1 year'));
                } else {
                    echo $this->Time->timeAgoInWords($post_search['Post']['modified'],
                    array(
                    'end' => '1 year'));
                }
            ?>
        </u>
        <br>
        
        <i>@<?= $post_search['User']['username'] ?></i>
        </div>
        <div class="post">
            <p class="text-left">
                <?= $post_search['Post']['body'] ?>
                <br>
                <?php 
                if ($post_search['Post']['image_src']) {
                    echo $this->Html->image(
                        'uploads/' . $post_search['Post']['image_src'], 
                        array(
                        'alt' => 'Image Post',
                        'height' => 300,
                        'style' => 'border-radius:10px;'
                    ));
                }
                ?>
            </p>
            <br>
            <?php
if ($post_search['Post']['is_a_share'] == 1) {
    for ($i=0;$i<count($shares);$i++) {
        if ($post_search['Post']['shared_id'] == $shares[$i]['Share']['id']) { ?>
        <div class="shared_cont">
        <div class="posted_by">
            <?php if ($shares[$i]['original_user_post']['User']['profile_picture']
            == 'default.png') { ?>
            <?= $this->Html->image(
            $shares[$i]['original_user_post']['User']['profile_picture'],
            array(
                'alt' => 'CakePHP',
                'height' => 50,
                'width' => 50,
                'style' => 'border-radius: 10%;float:left;
                    margin-right:10px;border:1px solid black;'
                )) ?>
            <?php } else { ?>
            <?= $this->Html->image('uploads/' . 
            $shares[$i]['original_user_post']['User']['profile_picture'],
            array(
                'alt' => 'CakePHP',
                'height' => 50,
                'width' => 50,
                'style' => 'border-radius: 10%;float:left;
                    margin-right:10px;border:1px solid black;'
                )) ?>
            <?php } ?>
            <b>
                <?= $this->Html->link(
                $shares[$i]['original_user_post']['User']['full_name'],
                array(
                    'controller' => 'users',
                    'action' => 'view',
                    $shares[$i]['original_user_post']['User']['id']
                ), array(
                    'style' => 'color:#003d4c;'
                )) ?>
            </b> &#8226;
            
            <u class="text-muted">
                <?php
                if (strtotime('-365 day') > strtotime($shares[$i]['Post']['modified'])) {
                    echo $this->Time->timeAgoInWords($shares[$i]['Post']['modified'],
                    array(
                    'accuracy' => array('year' => 'year'),
                    'end' => '1 year'));
                } elseif (strtotime('-30 day') > strtotime($shares[$i]['Post']['modified'])) {
                    echo $this->Time->timeAgoInWords($shares[$i]['Post']['modified'],
                    array(
                    'accuracy' => array('month' => 'month'),
                    'end' => '1 year'));
                } elseif (strtotime('-7 day') > strtotime($shares[$i]['Post']['modified'])) {
                    echo $this->Time->timeAgoInWords($shares[$i]['Post']['modified'],
                    array(
                    'accuracy' => array('week' => 'week'),
                    'end' => '1 year'));
                } elseif (strtotime('-1 day') > strtotime($shares[$i]['Post']['modified'])) {
                    echo $this->Time->timeAgoInWords($shares[$i]['Post']['modified'],
                    array(
                    'accuracy' => array('day' => 'day'),
                    'end' => '1 year'));
                } elseif (strtotime('-1 hour') > strtotime($shares[$i]['Post']['modified'])) {
                    echo $this->Time->timeAgoInWords($shares[$i]['Post']['modified'],
                    array(
                    'accuracy' => array('hour' => 'hour'),
                    'end' => '1 year'));
                } else {
                    echo $this->Time->timeAgoInWords($shares[$i]['Post']['modified'],
                    array(
                    'end' => '1 year'));
                }
            ?>
            </u>
            <?= $this->Html->link(
            $this->Html->tag('i', '', array(
            'class' => 'fas fa-user-plus')), array(
            'controller' => 'posts',
            'action' => 'edit',
            $post_search['Post']['id']
            ), array(
            'escape' => false,
            'class' => 'text-muted float-right h6'
            )) ?>
            <br>
            
            <i>@<?= $shares[$i]['original_user_post']['User']['username'] ?></i>
        </div>
        <div class="post">
        <p class="text-left">
            <?= $shares[$i]['Post']['body'] ?>
            <br>
            <?php 
                if ($shares[$i]['Post']['image_src']) {
                    echo $this->Html->image('uploads/' . $shares[$i]['Post']['image_src'], array(
                        'alt' => 'Image Post',
                        'height' => 300,
                        'style' => 'border-radius:10px;'
                    ));
                }
            ?>
        </p>
        </div>
        </div>
        <?php }}} ?>
        </div>
        <div class="lcs_actions">
        <!-- Likes Section -->
        <span>
            <?php
            if ($post_search['extra']['like_by_me']) {
                echo $this->Form->button(
                    $this->Html->tag('i','', array(
                        'class' => 'fas fa-heart')),
                    array(
                        'type' => 'button',
                        'class' => 'btn btn-md actionsBtn flat',
                        'id' => 'nl'.$post_search['Post']['id'],
                        'style' => 'background-color: transparent;',
                        'escape' => false
                    ));
            } else {
                echo $this->Form->button(
                    $this->Html->tag('i','', array(
                        'class' => 'far fa-heart')),
                    array(
                        'type' => 'button',
                        'class' => 'btn btn-md actionsBtn flat likeBtn',
                        'id' => 'nl'.$post_search['Post']['id'],
                        'style' => 'background-color: transparent;',
                        'onclick' => "liked(".$post_search['Post']['id'].",'$search_keyword')",
                        'escape' => false
                    ));
            } ?>
            <?= $this->Form->button(
                    $this->Html->tag('i','',
                    array(
                        'class' => 'fas fa-heart')),
                array(
                    'type' => 'button',
                    'class' => 'btn btn-md actionsBtn flat',
                    'id' => 'l'.$post_search['Post']['id'],
                    'style' => 'background-color: transparent;display: none;',
                    'escape' => false
                )); ?>

            <p class="like_count<?=$post_search['Post']['id']?>" 
            style="padding:0 !important;font-size:15px;">
                <?= $post_search['Post']['like_count'] ? $post_search['Post']['like_count'] : '' ?>
            </p>
        </span>

        <!-- Comment Section -->
        <span>
            <?=
            $this->Form->button(
                $this->Html->tag('i','', array(
                    'class' => 'fas fa-comments-alt')),
                array(
                    'type' => 'button',
                    'class' => 'btn btn-md flat actionsBtn commentBtn',
                    'style' => 'background-color: transparent;',
                    'onclick' => 'comment('.$post_search['Post']['id'].')',
                    'escape' => false,
                ));
            ?>

            <p style="padding:0;font-size:15px;">
                <?= $post_search['Post']['comment_count'] ? $post_search['Post']['comment_count'] : '' ?>
            </p>
        </span>

        <!-- Share Section -->
        <span>
        <?php if ($post_search['Post']['is_a_share'] == 1) { ?>
            <?= $this->Html->link(
                $this->Html->tag('i','', array(
                    'class' => 'fas fa-retweet')), array(
                        'controller' => 'posts',
                        'action' => 'share',
                        $post_search['share_info']['id']
                        ), 
                array(
                    'escape' => false,
                    'class' => 'btn btn-md'
                ));
            ?>
        <?php } else { ?>
            <?= $this->Html->link(
                $this->Html->tag('i','', array(
                    'class' => 'fas fa-retweet')), array(
                        'controller' => 'posts',
                        'action' => 'share',
                        $post_search['Post']['id']
                        ), 
                array(
                    'escape' => false,
                    'class' => 'btn btn-md'
                ));
            ?>
        <?php } ?>
            
            <p style="padding:0;font-size:15px;">
            <?= $post_search['Post']['share_count'] ? $post_search['Post']['share_count'] : '' ?>
            </p>
        </span>

        <div class="comments<?=$post_search['Post']['id']?>" style="display:none;"></div>
        <?= $this->Form->create('Comment', array(
            'class' => 'addComment'.$post_search['Post']['id'],
            'style' => 'display: none;'
        )) ?>
        <div class="row" style="width:100%;margin:auto;">
            <div class="col-7">
                <?= $this->Form->input(
                'comment', array(
                'label' => false,
                'class' => 'form-control commentBody'.$post_search['Post']['id'],
                'type' => 'text',
                'placeholder' => 'Enter comment here'
                )) ?>
            </div>

            <div class="col-0">
                <?= $this->Form->button(
                'Comment', array(
                'label' => false,
                'class' => 'btn btn-primary addCommentBtn',
                'onclick' => 'addComment('.$post_search['Post']['id'].')',
                'type' => 'button'
                )) ?>
            </div>
        </div>
    <?= $this->Form->end() ?>
    </div>
        </div>
    <?php endforeach;?>
    <?php unset($post_search); ?>
    <p>
        <?php if (count($post_searches) > 9) {?>
        <?= $this->Form->postLink(
        'See more...', array(
            'controller' => 'searches',
            'action' => 'search_posts',
            $search_keyword
        ), array(
            'style' => 'color:#003d4c;float:right;'
        )) ?>
    </p>
    <?php }} ?>
   </div>
</div>

<!-- AJAX script -->
<script>
function liked(id, keyword)
{
    $(document).ready(function() {
    $.ajax({
    type: 'POST',
    url: '/likes/likes/' + id,
    data: {id: id, keyword: keyword},
    dataType: 'HTML',
    headers: {'X-Requested-With': 'XMLHttpRequest'},
    success: function(response) {
        $('#nl'+id).hide();
        $('.like_count'+id).html(response);
        $('#l'+id).show();
    }
    });
    });
}

function comment(id)
{
    $(document).ready(function() {
    $.ajax({
    type: 'GET',
    url: '/posts/comment/' + id,
    dataType: 'HTML',
    headers: {'X-Requested-With': 'XMLHttpRequest'},
    success: function(response) {
        $('.comments'+id).show();
        $('.addComment'+id).show();
        $('.comments'+id).html(response);
    }
    });
    });
}

function addComment(id)
{
let commentBody = $('.commentBody'+id).val();
$(document).ready(function() {
    $.ajax({
    type: 'POST',
    url: '/posts/addComment/' + id,
    data: {comment: commentBody},
    dataType: 'HTML',
    headers: {'X-Requested-With': 'XMLHttpRequest'},
    success: function(response) {
        $('.comments'+id).show();
        $('.comments'+id).html(response);
    }
    });
});
}
</script>