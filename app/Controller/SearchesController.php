<?php

App::uses('AppController', 'Controller');
App::uses('CakeEmail', 'Network/Email');
App::uses('CakeTime', 'Utility');

/**
 * Searches Controller
 * 
 * Controller handles the search related functions.
 * 
 */
class SearchesController extends AppController
{
    var $uses = false;
    public $helpers = array('Html', 'Form', 'Flash');
    public $components = array('Flash', 'Paginator');
    
    public function beforeFilter()
    {
        parent::beforeFilter();
        $this->Auth->allow('search', 'search_users', 'search_posts');
    }

    public $paginate = array(
        'limit' => 10,
        'order' => array(
            'modified' => 'desc'
        )
    );
    /**
     * Displays data based on the keyword
     * 
     * @param string $keyword; Keyword to find on posts or users
     */
    public function search($keyword = null)
    {   
        $keyword = $keyword ? $keyword : $this->request->data['Search']['keywords'];
        $this->Paginator->settings = $this->paginate;
        $this->loadModel('Post');
        $results_post = $this->Post->find('all', array(
            'conditions' => array(
                'OR' => array(
                        'first_name LIKE' => 
                            '%'.$keyword.'%',
                        'body LIKE' =>
                            '%'.$keyword.'%',
                        'username LIKE' =>
                            '%'.$keyword.'%'
                ),
                'Post.is_active =' => 1
            ),
            'limit' => 10,
            'order' => array(
                'Post.modified' => 'desc'
            )
        ));
        $this->loadModel('Share');
        $share = $this->Share->find('all');
        for ($i=0;$i<count($results_post);$i++) {
            $results_post[$i]['extra'] = array(
                'like_by_me' => false,
                'like_id' => null
            );
            $results_post[$i]['share_info'] = array(
                'share_id' => null
            );
            for ($j=0;$j<count($results_post[$i]['Like']);$j++) {
                if ($results_post[$i]['Like'][$j]['user_id']
                == $this->Auth->user('id')) {
                    $results_post[$i]['extra'] = array(
                        'like_by_me' => true,
                        'like_id' => $results_post[$i]['Like'][$j]['id']
                    );
                }
            }
            $results_post[$i]['User']['full_name'] = 
                $results_post[$i]['User']['first_name'].' '.$results_post[$i]['User']['last_name'];
        }
        for ($i=0;$i<count($share);$i++) {
            for ($j=0;$j<count($results_post);$j++) {
                if ($results_post[$j]['Post']['is_a_share'] == 1) {
                    if ($share[$i]['Share']['id'] == $results_post[$j]['Post']['shared_id']) {
                        $share[$i]['original_user_post'] = $this->Post->find('first', array(
                            'conditions' => array(
                                'Post.id' => $share[$i]['Share']['post_id']
                            )
                        ));
                        $share[$i]['original_user_post']['User']['full_name'] =
                            $share[$i]['original_user_post']['User']['first_name'].
                            ' '.$share[$i]['original_user_post']['User']['last_name'];
                        
                        $results_post[$j]['share_info'] = $share[$i]['original_user_post']['Post'];
                    }
                }
            }
        }
        $this->loadModel('User');
        $results_user = $this->Paginator->paginate('User', array(
                    'OR' => array(
                        'first_name LIKE' => 
                            '%'.$keyword.'%',
                        'username LIKE' =>
                            '%'.$keyword.'%',
                        'last_name LIKE' =>
                            '%'.$keyword.'%'
                    )
            ));
        for ($i=0;$i<count($results_user);$i++) {
            $results_user[$i]['User']['full_name'] = 
                $results_user[$i]['User']['first_name'].' '.$results_user[$i]['User']['last_name'];
        }
        $this->loadModel('Follow');
        if (count($results_user) >= 1) {
            for ($i=0;$i<count($results_user);$i++) {
                $results_user[$i]['Count']['followers'] =
                $this->Follow->find('count', array(
                    'conditions' => array(
                'Follow.following' => $results_user[$i]['User']['id']
                )));
            }
        }
        $this->set('authUser', $this->Auth->user());
        $this->set('user_searches', $results_user);
        $this->set('search_keyword', $keyword);
        $this->set('post_searches', $results_post);
        $this->set('shares', $share);
        $this->render('/Searches/index');
    }
    /**
     * Displays all user related searches
     * 
     * @param string $keyword; Keyword to find in users
     */
    public function search_users($keyword)
    {
        $this->Paginator->settings = $this->paginate;
        $this->loadModel('User');
        $results_user = $this->Paginator->paginate('User', array(
                    'OR' => array(
                        'first_name LIKE' => 
                            '%'.$keyword.'%',
                        'username LIKE' =>
                            '%'.$keyword.'%'
                    )
            ));

        $this->set('authUser', $this->Auth->user());
        $this->set('user_searches', $results_user);
        $this->set('search_keyword', $keyword);
    }
    /**
     * Displays all posts related based on keyword
     * 
     * @param string $keyword; Keyword of post to find
     */
    public function search_posts($keyword)
    {
        $this->Paginator->settings = $this->paginate;
        $this->loadModel('Post');
        $results_post = $this->Paginator->paginate('Post', array(
                    'OR' => array(
                        'first_name LIKE' => 
                            '%'.$keyword.'%',
                        'body LIKE' =>
                            '%'.$keyword.'%',
                        'username LIKE' =>
                            '%'.$keyword.'%'
                    ),
                    'Post.is_active =' => 1
            ));
            $this->loadModel('Share');
            $share = $this->Share->find('all');
            for ($i=0;$i<count($results_post);$i++) {
                $results_post[$i]['extra'] = array(
                    'like_by_me' => false,
                    'like_id' => null
                );
                $results_post[$i]['share_info'] = array(
                    'share_id' => null
                );
                for ($j=0;$j<count($results_post[$i]['Like']);$j++) {
                    if ($results_post[$i]['Like'][$j]['user_id']
                    == $this->Auth->user('id')) {
                        $results_post[$i]['extra'] = array(
                            'like_by_me' => true,
                            'like_id' => $results_post[$i]['Like'][$j]['id']
                        );
                    }
                }
                $results_post[$i]['Post']['body'] = 
                htmlspecialchars_decode($results_post[$i]['Post']['body'], ENT_NOQUOTES);
                $results_post[$i]['User']['full_name'] = 
                    $results_post[$i]['User']['first_name'].' '.$results_post[$i]['User']['last_name'];
            }
            for ($i=0;$i<count($share);$i++) {
                for ($j=0;$j<count($results_post);$j++) {
                    if ($results_post[$j]['Post']['is_a_share'] == 1) {
                        if ($share[$i]['Share']['id'] == $results_post[$j]['Post']['shared_id']) {
                            $share[$i]['original_user_post'] = $this->Post->find('first', array(
                                'conditions' => array(
                                    'Post.id' => $share[$i]['Share']['post_id']
                                )
                            ));
                            $share[$i]['original_user_post']['User']['full_name'] =
                                $share[$i]['original_user_post']['User']['first_name'].
                                ' '.$share[$i]['original_user_post']['User']['last_name'];
                            
                            $results_post[$j]['share_info'] = $share[$i]['original_user_post']['Post'];
                        }
                    }
                }
            }
        $this->set('authUser', $this->Auth->user());
        $this->set('post_searches', $results_post);
        $this->set('search_keyword', $keyword);
        $this->set('shares', $share);
    }
}