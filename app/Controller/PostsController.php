<?php

App::uses('CakeTime', 'Utility');

/**
 * Posts Controller
 * 
 * This controller is responsible for posts related operations.
 * 
 */
class PostsController extends AppController
{
    public $helpers = array('Html', 'Form', 'Flash');
    public $components = array('Flash', 'Paginator');
    
    public $paginate = array(
        'limit' => 10,
        'order' => array(
            'Post.modified' => 'desc'
        )
    );

    public function isAuthorized($user)
    {
        if (in_array($this->action, array('add', 'likes', 'comment', 'share', 'addComment'))) {
            return true;
        }
        if (in_array($this->action, array('edit', 'delete', 'view'))) {
            $postId = (int) $this->request->params['pass'][0];
            if ($this->Post->isOwnedBy($postId, $user['id'])) {
                return true;
            }
        }
        return false;
    }
    /**
     * An action that is called to display all followed users posts 
     * 
     */
    public function index()
    {
        $this->Paginator->settings = $this->paginate;

        $this->loadModel('Share');
        $share = $this->Share->find('all');

        $this->loadModel('Follow');
        $following = $this->Follow->find('all',
        array(
            'conditions' => array(
                'user_id' => $this->Auth->user('id')
            )
        ));
        $arr = array();
        for ($i=0;$i<count($following);$i++) {
            array_push($arr, $following[$i]['Follow']['following']);
        }
        
        if ($following) {
                $data = $this->Paginator->paginate('Post',
                array(
                    'Post.is_active =' => 1,
                    'OR' => array(
                        array(
                            'Post.user_id' => $arr,
                            ),
                            'Post.user_id' => $this->Auth->user('id'),
                        )
                ));
        } else {
            $data = $this->Paginator->paginate('Post',
            array(
                'Post.is_active =' => 1,
                'Post.user_id' => $this->Auth->user('id'),
            ));
        }

        for ($i=0;$i<count($data);$i++) {
            $data[$i]['extra'] = array(
                'like_by_me' => false,
                'like_id' => null
            );
            $data[$i]['share_info'] = array(
                'share_id' => null
            );
            for ($j=0;$j<count($data[$i]['Like']);$j++) {
                if ($data[$i]['Like'][$j]['user_id']
                == $this->Auth->user('id')) {
                    $data[$i]['extra'] = array(
                        'like_by_me' => true,
                        'like_id' => $data[$i]['Like'][$j]['id']
                    );
                }
            }
            $data[$i]['User']['full_name'] = 
                $data[$i]['User']['first_name'].' '.$data[$i]['User']['last_name'];
        }
        for ($i=0;$i<count($share);$i++) {
            for ($j=0;$j<count($data);$j++) {
                if ($data[$j]['Post']['is_a_share'] == 1) {
                    if ($share[$i]['Share']['id'] == $data[$j]['Post']['shared_id']) {
                        $share[$i]['original_user_post'] = $this->Post->find('first', array(
                            'conditions' => array(
                                'Post.id' => $share[$i]['Share']['post_id']
                            )
                        ));
                        $share[$i]['original_user_post']['User']['full_name'] =
                            $share[$i]['original_user_post']['User']['first_name'].
                            ' '.$share[$i]['original_user_post']['User']['last_name'];
                        
                        $data[$j]['share_info'] = $share[$i]['original_user_post']['Post'];
                    }
                }
            }
        }
        $this->set('authUser', $this->Auth->user());
        $this->set('shares', $share);
        $this->set('posts', $data);
    }
    /**
     * View owned posts
     * 
     * @param int $id; The id of the logged in user
     */
    public function view($id = null)
    {
        $this->Paginator->settings = $this->paginate;

        $data = $this->Paginator->paginate('Post', array(
                'AND' => array(
                    'Post.user_id =' => $id,
                    'Post.is_active =' => 1
                )
            ));
        $this->loadModel('Share');
        $share = $this->Share->find('all');
        for ($i=0;$i<count($share);$i++) {
            for ($j=0;$j<count($data);$j++) {
                if ($data[$j]['Post']['is_a_share'] == 1) {
                    if ($share[$i]['Share']['id'] == $data[$j]['Post']['shared_id']) {
                        $share[$i]['original_user_post'] = $this->Post->find('first', array(
                            'conditions' => array(
                                'Post.id' => $share[$i]['Share']['post_id']
                            )
                        ));
                        $share[$i]['original_user_post']['User']['full_name'] =
                            $share[$i]['original_user_post']['User']['first_name'].
                            ' '.$share[$i]['original_user_post']['User']['last_name'];
                        
                        $data[$j]['share_info'] = $share[$i]['original_user_post']['Post'];
                    }
                }
            }
        }
        $this->set('posts', $data);
        $this->set('shares', $share);
    }
    /**
     * Add new posts
     * 
     * @return void 
     */
    public function add()
    {
        if ($this->request->is('post')) {
            $this->Post->create();
            $this->request->data['Post']['user_id'] = $this->Auth->user('id');

            if ($this->Post->isUploadedFile($this->request->data['Post']['image'])) {
                $tmp = $this->request->data['Post']['image']['tmp_name'];
                $target = WWW_ROOT.'img'.DS.'uploads'.DS;
                
                $image = CakeTime::convert(time(), new DateTimeZone('Asia/Jakarta')).
                Security::hash($this->request->data['Post']['image']['name']);
                
                $target = $target.basename($image);
            
                $this->request->data['Post']['image_src'] = $image;

                move_uploaded_file($tmp, $target);
            }
            
            if ($this->Post->save($this->request->data)) {
                $this->Flash->success(__('Your post has been created.'));
                return $this->redirect(array('action' => 'index'));
            }
            $this->Flash->error(__('Unable to create your post.'));
        }
    }
    /**
     * Edits owned post
     * 
     * @param int $id; Id of the post to be edited
     */
    public function edit($id = null)
    {
        if (!$id) {
            throw new NotFoundException(__('Invalid post'));
        }

        $post = $this->Post->findById($id);
        $post['Post']['body'] = htmlspecialchars_decode($post['Post']['body'], ENT_NOQUOTES);
        if (!$post) {
            throw new NotFoundException(__('Invalid post'));
        }

        if ($this->request->is(array('post', 'put'))) {
            $this->Post->id = $id;

            if ($this->Post->isUploadedFile($this->request->data['Post']['image'])) {
                $tmp = $this->request->data['Post']['image']['tmp_name'];
                $target = WWW_ROOT.'img'.DS.'uploads'.DS;
                
                $image = CakeTime::convert(time(), new DateTimeZone('Asia/Jakarta')).
                Security::hash($this->request->data['Post']['image']['name']);
                
                $target = $target.basename($image);
            
                $this->request->data['Post']['image_src'] = $image;

                move_uploaded_file($tmp, $target);
            }

            if ($this->Post->save($this->request->data)) {
                $this->Flash->success(__('Your post has been updated.'));
                return $this->redirect(array(
                    'action' => 'index/'.$this->Auth->user('id')));
                // return $this->redirect($this->referer());
            }
            $this->Flash->error(__('Unable to update your post.'));
        }

        $this->set('posts', $post);
        if (!$this->request->data) {
            $this->request->data = $post;
        }
    }
    /**
     * Deletes an owned post
     * 
     * @param int $id; Id of the post to be deleted
     */
    public function delete($id)
    {
        if ($this->request->is('get')) {
            throw new MethodNotAllowedException();
        }
        $this->Post->id = $id;

        if ($this->Post->saveField('is_active', 0)) {
            $this->Flash->success(
                __('The post has been deleted.')
            );
            return $this->redirect($this->referer());
        } else {
            $this->Flash->error(
                __('The post could not be deleted.')
            );
        }
 
        return $this->redirect(array('action' => 'view'));
    }
    /**
     * Display comments of a particular post
     * 
     * @param int $id; Id of the post
     */
    public function comment($id = null)
    {
        $this->autoRender = false;
        if (!$id) {
            throw new NotFoundException(__('Invalid post'));
        }
        $this->loadModel('Comment');
        $post = $this->Post->findById($id);
        $comment = $this->Comment->find(
            'all',
            array(
            'conditions' => array(
            'Comment.post_id =' => $id
            ),
            'order' => array(
                'Comment.modified' => 'asc')));
        
        for ($i=0;$i<count($comment);$i++) {
            $comment[$i]['User']['full_name'] = 
                $comment[$i]['User']['first_name'].' '.$comment[$i]['User']['last_name'];
        }
        $this->layout = null;
        $this->set('comments', $comment);
        $this->render('ajax/comments');
    }
    /**
     * Controller action called for adding a comment
     * 
     * @param int $id; Id of the posts the comment will
     * be added.
     */
    public function addComment($id = null)
    {
        $this->loadModel('Comment');
        if ($this->request->is(array('post', 'put'))) {
            $this->request->data['Comment']['post_id'] = $id;
            $this->request->data['Comment']['user_id'] = $this->Auth->user('id');
            $this->request->data['Comment']['comment'] = $this->request->data['comment'];
            $this->Comment->create();
            if ($this->Comment->save($this->request->data['Comment'])) {
                $comment = $this->Comment->find(
                    'all',
                    array(
                    'conditions' => array(
                    'Comment.post_id =' => $id
                    ),
                    'order' => array(
                        'Comment.modified' => 'asc'
                    )
                    ));
                for ($i=0;$i<count($comment);$i++) {
                    $comment[$i]['User']['full_name'] = 
                        $comment[$i]['User']['first_name'].' '.$comment[$i]['User']['last_name'];
                }
                $this->set('comments', $comment);
                $this->layout = null;
                $this->render('ajax/comments');
            }
        }
    }
    /**
     * Shares the original post
     * 
     * @param int $id; Id of the post to be shared
     */
    public function share($id = null)
    {
        if (!$id) {
            throw new NotFoundException(__('Invalid post'));
        }

        $post = $this->Post->findById($id);
        if (!$post) {
            throw new NotFoundException(__('Invalid post'));
        }

        if ($this->request->is(array('post'))) {
            $this->Post->id = $id;

            $this->loadModel('Share');
            $this->request->data['Share']['post_id'] = $id;
            $this->request->data['Share']['user_id'] = $this->Auth->user('id');

            if ($this->Share->save($this->request->data)) {
                $this->Post->create();
                $this->request->data['Post']['shared_id'] = $this->Share->getLastInsertId();
                $this->request->data['Post']['user_id'] = $this->Auth->user('id');
                $this->request->data['Post']['is_a_share'] = 1;
                $this->Post->save($this->request->data);
                return $this->redirect(array('action' => 'index'));
            }
            $this->Flash->error(__('Unable to update your post.'));
        }
        $this->set('posts', $post);
    }
}