<?php

App::uses('CakeTime', 'Utility');
// App::import('Model', 'Post');

class CommentsController extends AppController
{
    public $helpers = array('Html', 'Form', 'Flash');
    public $components = array('Flash', 'Paginator');

    public $paginate = array(
        'limit' => 10,
        'order' => array(
            'Post.modified' => 'desc'
        )
    );

    public function isAuthorized($user)
    {
        if (in_array($this->action, array('add', 'likes', 'unlikes', 'comment'))) {
            return true;
        }

        if (in_array($this->action, array('edit', 'delete'))) {
            $postId = (int) $this->request->params['pass'][0];
            if ($this->Post->isOwnedBy($postId, $user['id'])) {
                return true;
            }
        }

        return false;
    }
}