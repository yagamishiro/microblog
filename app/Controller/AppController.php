<?php

App::uses('Controller', 'Controller');

class AppController extends Controller
{

    public $components = array(
        'DebugKit.Toolbar',
        'Flash',
        'RequestHandler',
        'Auth' => array(
            'loginRedirect' => array(
                'controller' => 'posts',
                'action' => 'index'
            ),
            'logoutRedirect' => array(
                'controller' => 'pages',
                'action' => 'display',
                'home'
            ),
            'authenticate' => array(
                'Form' => array(
                    'passwordHasher' => 'Blowfish'
                )
            ),
            'authorize' => array('Controller')
        )
    );

    public function beforeFilter()
    {
        $this->Auth->allow('index', 'view');
    }
}