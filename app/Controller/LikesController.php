<?php

App::uses('CakeTime', 'Utility');

class LikesController extends AppController
{
    public $helpers = array('Html', 'Form', 'Flash');
    public $components = array('Flash', 'Paginator', 'RequestHandler');

    public $paginate = array(
        'limit' => 10,
        'order' => array(
            'Post.modified' => 'desc'
        )
    );
    public function isAuthorized($user)
    {
        if (in_array($this->action, array('add', 'likes', 'unlikes'))) {
            return true;
        }
        if (in_array($this->action, array('edit', 'delete'))) {
            $postId = (int) $this->request->params['pass'][0];
            if ($this->Post->isOwnedBy($postId, $user['id'])) {
                return true;
            }
        }
        return false;
    }
    /**
     * Likes a post
     * 
     * @param int $id; Id of the post you want to like
     * @return int count_like
     */
    public function likes($id)
    {
        
        $this->autoRender = false;
        
        if ($this->request->is('get')) {
            throw new MethodNotAllowedException();
        }
        $this->request->data['Like']['post_id'] = $id;
        $this->request->data['Like']['user_id'] = $this->Auth->user('id');
        $this->Like->create();
        $this->Like->save($this->request->data);
        $this->loadModel('Post');
        $results_post = $this->Post->find('first', array(
            'conditions' => array(
                'Post.is_active =' => 1
            ),
        ));
        return $results_post['Post']['like_count'];
    }
    // public function unlikes($id)
    // {
    //     if ($this->request->is('get')) {
    //         throw new MethodNotAllowedException();
    //     }
    //     if ($this->Like->delete($id)) {
    //     } else {
    //         $this->Flash->error(
    //             __('The post with id: %s could not be unlike.', h($id))
    //         );
    //     }
    //     return $this->redirect(
    //         array(
    //             'controller' => 'posts',
    //             'action' => 'index'
    //     ));
    // }
}