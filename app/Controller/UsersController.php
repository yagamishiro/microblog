<?php

App::uses('AppController', 'Controller');
App::uses('CakeEmail', 'Network/Email');
App::uses('CakeTime', 'Utility');

class UsersController extends AppController
{
    public $helpers = array('Html', 'Form', 'Flash');
    public $components = array('Flash', 'Paginator');
    
    public function beforeFilter()
    {
        parent::beforeFilter();
        $this->Auth->allow(
        'add', 'logout', 'activate', 'search', 'login','view',
        'view_follower','view_following','forgot_password', 'reset_password');
    }
    public function isAuthorized($user)
    {
        if (in_array($this->action, array('search','view','view_follower','view_following'))) {
            return true;
        }
        if (in_array($this->action, array(
            '/', 'edit', 'change_password', 'follow', 'unfollow', 'reactivate'))) {
            $userId = (int) $this->request->params['pass'][0];
            if ($this->User->isOwnedBy($userId)) {
                return true;
            }
        }
        return false;
    }
    /**
     * Action that gives a user authority to use Microblog
     * 
     */
    public function login()
    {
        if ($this->request->is('post')) {
            if ($this->Auth->login()) {
                if ($this->Auth->user('status') == 1) {
                    return $this->redirect($this->Auth->redirectUrl());
                } elseif ($this->Auth->user('status') == 0) {
                    $this->Flash->error(
                    __('Account not yet activated. Check your email for the activation.'));
                }
            }
            $this->Flash->error(__('Invalid username or password, please try again.'));
        }
    }
    /**
     * Exits the user from the Microblog
     * 
     */
    public function logout()
    {
        $this->Auth->logout();
        return $this->redirect(array(
            'controller' => 'users',
            'action' => 'login'
        ));
    }
    
    public function index()
    {
        $this->User->recursive = 0;
        $this->set('users', $this->paginate());
    }

    public $paginate = array(
        'limit' => 6,
        'order' => array(
            'modified' => 'desc'
        )
    );
    /**
     * The view action that renders the profile of 
     * the logged in user or other users
     * 
     * @param int|null $id; Id of the user want to display
     */
    public function view($id = null)
    {
        $this->User->id = $id;
        if (!$this->User->exists()) {
            throw new NotFoundException(__('Invalid user'));
        }
        $user = $this->User->findById($id);
        $user['User']['full_name'] = 
        $user['User']['first_name'] . ' ' . $user['User']['last_name'];
        
        $this->loadModel('Post');
        $user['Count']['post_count'] = $this->Post->find('count', array(
            'conditions' => array(
                'Post.is_active' => 1,
                'Post.user_id' => $id
            )
        ));
        $this->loadModel('Follow');
        $user['Count']['followed_by_me'] = $this->Follow->find('count', array(
            'conditions' => array(
                'Follow.user_id' => $this->Auth->user('id'),
                'Follow.following' => $id
            )
        ));
        $user['Count']['followers'] = $this->Follow->find('count', array(
            'conditions' => array(
                'Follow.following' => $id
            )
        ));
        $this->Paginator->settings = $this->paginate;

        $data = $this->Paginator->paginate('Post', array(
            'Post.is_active =' => 1,
            'Post.user_id' => $id
        ));
        $this->loadModel('Share');
        $share = $this->Share->find('all');
        for ($i=0;$i<count($data);$i++) {
            $data[$i]['User']['full_name'] = 
                $data[$i]['User']['first_name'].' '.$data[$i]['User']['last_name'];
        }
        for ($i=0;$i<count($share);$i++) {
            for ($j=0;$j<count($data);$j++) {
                if ($data[$j]['Post']['is_a_share'] == 1) {
                    if ($share[$i]['Share']['id'] == $data[$j]['Post']['shared_id']) {
                        $share[$i]['original_user_post'] = $this->Post->find('first', array(
                            'conditions' => array(
                                'Post.id' => $share[$i]['Share']['post_id']
                            )
                        ));
                        $share[$i]['original_user_post']['User']['full_name'] =
                            $share[$i]['original_user_post']['User']['first_name'].
                            ' '.$share[$i]['original_user_post']['User']['last_name'];
                        
                        $data[$j]['share_info'] = $share[$i]['original_user_post']['Post'];
                    }
                }
            }
        }
        $this->set('user', $user);
        $this->set('posts', $data);
        $this->set('shares', $share);
    }
    /**
     * View follower action that displays user's follower
     * 
     * @param int $id; Id of the user 
     */
    public function view_follower($id = null)
    {
        $this->User->id = $id;
        if (!$this->User->exists()) {
            throw new NotFoundException(__('Invalid user'));
        }

        $user = $this->User->findById($id);
        $user['User']['full_name'] = 
        $user['User']['first_name'] . ' ' . $user['User']['last_name'];
        
        $this->loadModel('Post');
        $user['Count']['post_count'] = $this->Post->find('count', array(
            'conditions' => array(
                'Post.is_active' => 1,
                'Post.user_id' => $id
            )
        ));
        $this->loadModel('Follow');
        $user['Count']['followed_by_me'] = $this->Follow->find('count', array(
            'conditions' => array(
                'Follow.user_id' => $this->Auth->user('id'),
                'Follow.following' => $id
            )
        ));
        $user['Count']['followers'] = $this->Follow->find('count', array(
            'conditions' => array(
                'Follow.following' => $id
            )
        ));
        $this->Paginator->settings = $this->paginate;
        $followers = $this->Paginator->paginate('Follow', array(
            'Follow.following' => $id
        ));
        $following = $this->Paginator->paginate('Follow', array(
            'Follow.user_id' => $this->Auth->user('id')
        ));
        if ($followers) {
            for ($i=0;$i<count($followers);$i++) {
            if ($following) {
                $followers[$i]['followed_by_me'] = 
                $followers[$i]['Follow']['following'] == $followers[$i]['User']['id'];
            } else {
                $followers[$i]['followed_by_me'] = false;
            }
            $followers[$i]['User']['full_name'] = 
            $followers[$i]['User']['first_name'].' '.$followers[$i]['User']['last_name'];
            }
        }
        $this->set('user', $user);
        $this->set('followers', $followers);
        $this->set('followings', $following);
    }
    /**
     * View action that displays who the user follows
     * 
     * @param int $id; Id of the user
     */
    public function view_following($id = null)
    {
        $this->User->id = $id;
        if (!$this->User->exists()) {
            throw new NotFoundException(__('Invalid user'));
        }

        $user = $this->User->findById($id);
        $this->loadModel('Post');
        $user['Count']['post_count'] = $this->Post->find('count', array(
            'conditions' => array(
                'Post.is_active' => 1,
                'Post.user_id' => $id
            )
        ));
        $user['User']['full_name'] = $user['User']['first_name'] . ' ' . $user['User']['last_name'];
        $this->loadModel('Follow');
        $user['Count']['followed_by_me'] = $this->Follow->find('count', array(
            'conditions' => array(
                'Follow.user_id' => $this->Auth->user('id'),
                'Follow.following' => $id
            )
        ));
        $user['Count']['followers'] = $this->Follow->find('count', array(
            'conditions' => array(
                'Follow.following' => $id
            )
        ));

        $this->Paginator->settings = $this->paginate;

        $followed_by_loggedin = $this->Paginator->paginate('Follow', array(
            'Follow.user_id' => $this->Auth->user('id')
        ));
        
        $following = $this->Paginator->paginate('Follow', array(
            'Follow.user_id' => $id
        ));

        if ($following) {
            for ($i=0;$i<count($following);$i++) {
            if ($following && !$followed_by_loggedin) {
                $following[$i]['followed_by_me_info'] = $this->User->find('first',
                array(
                    'conditions' => array(
                        'id' => $following[$i]['Follow']['following']
                    )));

                $following[$i]['followed_by_me_info']['User']['full_name'] = 
                $following[$i]['followed_by_me_info']['User']['first_name']. ' '.
                $following[$i]['followed_by_me_info']['User']['last_name'];

                $following[$i]['followed_by_me'] = false;
            } elseif ($following && $followed_by_loggedin) {
                $following[$i]['followed_by_me_info'] = $this->User->find('first',
                array(
                    'conditions' => array(
                        'id' => $following[$i]['Follow']['following']
                    )));

                $following[$i]['followed_by_me_info']['User']['full_name'] = 
                $following[$i]['followed_by_me_info']['User']['first_name']. ' '.
                $following[$i]['followed_by_me_info']['User']['last_name'];

                $following[$i]['followed_by_me'] = 
                $following[$i]['Follow']['following'] ==
                $followed_by_loggedin[$i]['Follow']['following'];
            }
            }
        }
        $this->set('user', $user);
        $this->set('followers', $followed_by_loggedin);
        $this->set('followings', $following);
    }
    /**
     * User registration
     * 
     * Adds a user that registered in the Microblog
     * 
     */
    public function add()
    {
        if ($this->request->is('post')) {
            $this->User->create();
            $this->request->data['User']['activation_key'] = $activation_key = time();
            if ($this->User->save($this->request->data)) {
                // $activationUrl = 
                // 'http://dev6.ynsdev.pw/users/activate/' . $activation_key;
                $activationUrl = Router::url(array(
                    'controller' => 'users',
                    'action' => 'activate'
                    ), true).'/'.$activation_key;

                $Email = new CakeEmail();
                $Email->emailFormat('html')
                    ->template('activation', 'mytemplate')
                    ->from(array('curioussince95@gmail.com' => 'Microblog'))
                    ->to($this->request->data['User']['email'])
                    ->subject('Microblog Activation Link')
                    ->viewVars(array('activationUrl' => $activationUrl))
                    ->send();
                $this->Flash->success(
                    __('Your account has been created.
                    Please verify it by clicking the activation link that has been
                    sent to your email.'));
                return $this->redirect(array('action' => 'add'));
            } else {
                $this->Flash->error(
                    __('The user could not be saved.Please try again.')
                );
            }
        }
        if($this->Auth->user()) {
            $this->redirect(array(
                'controller' => 'posts',
                'action' => 'index'
            ));
        }
    }
    /**
     * Activates a newly registered user
     * 
     * @return void
     */
    public function activate($activation_key)
    {
        $userData = $this->User->find('first', array(
           'conditions' => array(
               'User.activation_key' => $activation_key,
               'User.status' => 0
           ) 
        ));
        if (!empty($userData)) {

            $data = array('id' => $userData['User']['id'],
            'activation_key' => null,
            'status' => 1);
            $this->User->save($data);

            $this->Flash->success(
                __('Your account has been activated.
                You may now login to access your account.'));
            return $this->redirect(array('action' => 'login'));
        } else {
            $this->Flash->error(__('Your account is already activated.'));
            return $this->redirect(array('action' => 'login'));
        }
    }
    /**
     * Edit function renders the profile edit view and edits
     * user profile
     * 
     */
    public function edit($id = null)
    {
        $this->User->id = $this->Auth->user('id');
        if (!$this->User->exists()) {
            throw new NotFoundException(__('Invalid user'));
        }
        
        if ($this->request->is('post') || $this->request->is('put')) {

            $this->loadModel('Post');
            if ($this->request->data['User']['image'] && 
            $this->Post->isUploadedFile($this->request->data['User']['image'])) {
                $tmp = $this->request->data['User']['image']['tmp_name'];
                $target = WWW_ROOT.'img'.DS.'uploads'.DS;
                
                $image = CakeTime::convert(time(), new DateTimeZone('Asia/Jakarta')).
                Security::hash($this->request->data['User']['image']['name']);
                
                $target = $target.basename($image);
            
                $this->request->data['User']['profile_picture'] = $image;

                move_uploaded_file($tmp, $target);
            }

            $data =  $this->User->find('first',
                array(
                'conditions' => array(
                    'id' => $id
                )));
                
            if ($data['User']['email'] != $this->request->data['User']['email']) {
                $code = mt_rand(100000, 999999);
                $this->request->data['User']['status'] = 0;
                $this->request->data['User']['activation_key'] = $code;
                
                $Email = new CakeEmail();
                $Email->emailFormat('html')
                    ->template('reactivation', 'mytemplate')
                    ->from(array('curioussince95@gmail.com' => 'Microblog'))
                    ->to($this->request->data['User']['email'])
                    ->subject('Microblog Activation Link')
                    ->viewVars(array('activationUrl' => $code))
                    ->send();

                $this->User->save($this->request->data);
                return $this->redirect(array('action' => 'edit/'.$id));
            }

            if ($this->User->save($this->request->data)) {
                $this->Flash->success(__('Profile has been updated.'));
                return $this->redirect(array('action' => 'view/'.$id));
            }
            $this->Flash->error(
                __('The profile could not be updated. Please, try again.')
            );
        } else {
            $user = $this->request->data = $this->User->findById($id);
            $user['User']['reverify'] = false;
            $this->set('user', $user);
            unset($this->request->data['User']['password']);
        }
    }
    /**
     * Reactivate function is used when a user wants to change his/her
     * email.
     * 
     * @return void
     */
    public function reactivate()
    {
        $data =  $this->User->find('first',
                array(
                'conditions' => array(
                    'id' => $this->request->data['User']['id']
                )));
        if ($data['User']['activation_key'] == 
            $this->request->data['User']['verification_code']) {
            $this->request->data['User']['status'] = 1;
            $this->request->data['User']['activation_key'] = null;
            $this->User->save($this->request->data);

            $this->Flash->success(__('Email has been successfuly verified.'));
            return $this->redirect(array(
            'action' => 'edit/'.$this->request->data['User']['id']));
        }
        $this->Flash->error(
            __('Invalid verification code. Please, try again.')
        );
        return $this->redirect($this->referer());
    }
    /**
     * Renders a page where user can change his/her password
     * 
     * @param int $id; Id of the user
     * @return void
     */
    public function change_password($id = null)
    {
        $this->User->id = $this->Auth->user('id');
        if (!$this->User->exists()) {
            throw new NotFoundException(__('Invalid user'));
        }
        if ($this->request->is('post') || $this->request->is('put')) {
            if ($this->User->save($this->request->data)) {
                $this->Flash->success(
                __('Password has been updated. Please login
                with your new password.'));
                return $this->redirect(array('action' => 'login'));
            }
            $this->Flash->error(
                __('Password could not be updated. Please, try again.')
            );
        } else {
        $user = $this->request->data = $this->User->findById($id);
        $this->set('user', $user);
        unset($this->request->data['User']['password']);
        }
    }
    /**
     * Follows a user
     * 
     * @param int $id; Id of the user you want to follow
     */
    public function follow($id = null)
    {
        if ($this->request->is('post')) {
            $this->loadModel('Follow');
            $this->Follow->create();
            $this->request->data['Follow']['user_id'] = $this->Auth->user('id');
            $this->request->data['Follow']['following'] = $id;
            if ($this->Follow->save($this->request->data)) {
                return $this->redirect($this->referer());
            } else {
                $this->Flash->error(
                    __('The user could not be saved.Please try again.')
                );
            }
        }
    }
    /**
     * Unfollows a user
     * 
     * @param int $id; If of the user you want to unfollow
     */
    public function unfollow($id = null)
    {
        if ($this->request->is('get')) {
            throw new MethodNotAllowedException();
        }
        $this->loadModel('Follow');
        
        if ($this->Follow->deleteAll(
            array(
            'Follow.user_id' => $this->Auth->user('id'),
            'Follow.following' => $id
        ), true,
        true
        )) {
        } else {
            $this->Flash->error(
                __('The post with id: %s could not be unlike.', h($id))
            );
        }
        return $this->redirect($this->referer());
    }
    /**
     * Renders a view for forgot password 
     */
    public function forgot_password()
    {
        if (!empty($this->request->data)) {
            $email = $this->request->data['User']['email'];
            $data = $this->User->find('first', array(
                'conditions' => array(
                    'User.email' => $email
                )
            ));
            if (!empty($data)) {
                $this->request->data['User']['activation_key'] = $activation_key = time();
                $url = Router::url(array(
                    'controller' => 'users',
                    'action' => 'reset_password'
                    ), true).'/'.$activation_key;
                $this->User->id = $data['User']['id'];
                
                if ($this->User->saveField('activation_key', $activation_key)) {
                    $Email = new CakeEmail();
                    $Email->emailFormat('html')
                    ->template('reset_password', 'mytemplate')
                    ->from(array('curioussince95@gmail.com' => 'Microblog'))
                    ->to($data['User']['email'])
                    ->subject('Microblog Reset Password')
                    ->viewVars(array('activationUrl' => $url))
                    ->send();
                    $this->Flash->success(
                    __('Check Your Email To Reset your Password.'));
                }
            } else {
                $this->Flash->error(
                    __('Email entered does not exists.'));
            }
        }
    }
    /**
     * Renders a view for resetting the password
     * 
     * @param int $token; The token to know if link from
     * email is still valid
     */
    function reset_password($token = null)
    {
        $this->User->recursive = -1;
        if (!empty($token)) {
            $data = $this->User->find('first', array(
                'conditions' => array(
                    'activation_key' => $token
                )
            ));
            if ($data) {
                $this->User->id = $data['User']['id'];
                if (!empty($this->request->data)) {
                    $this->request->data['User']['activation_key'] = null;
                    if ($this->User->save($this->request->data)) {
                        $this->Flash->success(__('Password has been updated.'));
                        return $this->redirect(array('action' => 'login'));
                    }
                    $this->Flash->error(
                        __('Password could not be updated. Please, try again.')
                    );
                }
            }
        }
    }
}