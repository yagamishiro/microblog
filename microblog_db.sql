-- phpMyAdmin SQL Dump
-- version 4.4.15.10
-- https://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Feb 11, 2021 at 01:41 AM
-- Server version: 5.6.50
-- PHP Version: 5.6.40

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `microblog_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `comments`
--

CREATE TABLE IF NOT EXISTS `comments` (
  `id` int(11) NOT NULL,
  `post_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `comment` varchar(255) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `comments`
--

INSERT INTO `comments` (`id`, `post_id`, `user_id`, `comment`, `created`, `modified`) VALUES
(1, 17, 46, 'hey, this is my comment.', '2021-01-28 07:21:53', '2021-01-28 07:21:53'),
(2, 17, 46, 'hey, this is my another comment.', '2021-01-28 07:26:32', '2021-01-28 07:26:32'),
(3, 17, 45, 'hey, this is my another another comment.', '2021-01-28 07:26:59', '2021-01-28 07:26:59'),
(4, 17, 46, 'asdasdasdasdasda', '2021-01-28 08:52:27', '2021-01-28 08:52:27'),
(5, 25, 46, 'asdasdasd', '2021-01-28 09:08:21', '2021-01-28 09:08:21'),
(6, 22, 46, 'hey, this is my another comment.', '2021-01-28 09:14:41', '2021-01-28 09:14:41'),
(7, 25, 46, 'hahahahaah', '2021-01-29 08:22:33', '2021-01-29 08:22:33'),
(8, 35, 46, 'Hey! This is the first comment of the day.', '2021-02-03 03:41:59', '2021-02-03 03:41:59');

-- --------------------------------------------------------

--
-- Table structure for table `follows`
--

CREATE TABLE IF NOT EXISTS `follows` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `following` int(11) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `follows`
--

INSERT INTO `follows` (`id`, `user_id`, `following`, `created`, `modified`) VALUES
(13, 45, 46, '2021-02-02 08:13:32', '2021-02-02 08:13:32'),
(18, 46, 45, '2021-02-03 01:34:15', '2021-02-03 01:34:15'),
(19, 47, 46, '2021-02-03 05:25:41', '2021-02-03 05:25:41');

-- --------------------------------------------------------

--
-- Table structure for table `likes`
--

CREATE TABLE IF NOT EXISTS `likes` (
  `id` int(11) NOT NULL,
  `post_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `likes`
--

INSERT INTO `likes` (`id`, `post_id`, `user_id`, `created`, `modified`) VALUES
(1, 23, 46, '2021-01-28 01:51:43', '2021-01-28 01:51:43'),
(3, 23, 45, '2021-01-28 02:06:01', '2021-01-28 02:06:01'),
(7, 21, 46, '2021-01-28 02:43:19', '2021-01-28 02:43:19'),
(11, 24, 45, '2021-01-28 05:25:26', '2021-01-28 05:25:26'),
(12, 35, 46, '2021-02-03 03:42:22', '2021-02-03 03:42:22');

-- --------------------------------------------------------

--
-- Table structure for table `posts`
--

CREATE TABLE IF NOT EXISTS `posts` (
  `id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `body` text,
  `image_src` varchar(255) DEFAULT NULL,
  `is_active` tinyint(4) NOT NULL DEFAULT '1',
  `is_a_share` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0 is a post, 1 is a share',
  `shared_id` int(11) DEFAULT NULL,
  `share_count` int(11) NOT NULL DEFAULT '0',
  `like_count` int(11) NOT NULL DEFAULT '0',
  `comment_count` int(11) NOT NULL DEFAULT '0',
  `created` datetime NOT NULL,
  `modified` datetime DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=40 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `posts`
--

INSERT INTO `posts` (`id`, `user_id`, `body`, `image_src`, `is_active`, `is_a_share`, `shared_id`, `share_count`, `like_count`, `comment_count`, `created`, `modified`) VALUES
(1, 45, 'This is the first post of the microblogThis is the first post of the microblogThis is the first post of the microblogThis is the first post of the microblogThis is the first post of the microblog', '', 1, 0, 0, 0, 0, 0, '2021-01-21 12:02:00', '2021-01-21 12:02:00'),
(2, 45, 'This is the second blog of the microblog.', NULL, 1, 0, 0, 0, 1, 0, '2021-01-21 05:37:34', '2021-01-21 05:37:34'),
(3, 45, 'This is the third blog of the microblog.', NULL, 1, 0, 0, 1, 0, 0, '2021-01-21 05:38:16', '2021-01-21 05:38:16'),
(4, 46, 'Sdadasdasdasdasdasdasdasd', NULL, 1, 0, 0, 0, 0, 0, '2021-01-25 07:18:41', '2021-01-25 07:18:41'),
(5, 46, 'This is the fourth post in the microblog.', NULL, 1, 0, 0, 0, 0, 0, '2021-01-25 07:20:26', '2021-01-25 07:20:26'),
(6, 46, 'Sasdasdasdasdasdasdsdasdasdasdasdasdasdasdasdasdasdasdasd', NULL, 1, 0, 0, 0, 0, 0, '2021-01-25 07:23:48', '2021-01-25 07:23:48'),
(7, 46, 'This is yet another medyo long post in the microblog. I do not even know what I am writing right now.', NULL, 1, 0, 0, 0, 0, 0, '2021-01-25 09:37:38', '2021-01-25 09:37:38'),
(8, 46, 'Today''s sample post on my simple microblog.', NULL, 1, 0, 0, 0, 0, 0, '2021-01-26 02:35:25', '2021-01-26 02:35:25'),
(9, 46, '12345678901', NULL, 1, 0, 0, 0, 0, 0, '2021-01-26 03:54:28', '2021-01-26 03:54:28'),
(10, 46, 'example of post that is not capitalize', NULL, 1, 0, 0, 0, 0, 0, '2021-01-26 05:25:33', '2021-01-26 05:25:33'),
(11, 46, 'sample only', NULL, 1, 0, 0, 0, 0, 0, '2021-01-26 06:14:10', '2021-01-26 06:14:10'),
(12, 46, 'What is my thought?', NULL, 1, 0, 0, 0, 0, 0, '2021-01-26 06:15:35', '2021-01-26 06:15:35'),
(13, 46, 'signature', NULL, 1, 0, 0, 0, 0, 0, '2021-01-26 06:19:32', '2021-01-26 06:19:32'),
(14, 46, 'sadsdasd', NULL, 1, 0, 0, 0, 0, 0, '2021-01-26 06:20:25', '2021-01-26 06:20:25'),
(15, 46, 'asasdasd', NULL, 1, 0, 0, 0, 0, 0, '2021-01-26 06:21:08', '2021-01-26 06:21:08'),
(16, 46, 'sample nanaman', '', 1, 0, 0, 0, 0, 0, '2021-01-26 06:39:25', '2021-01-26 06:39:25'),
(17, 46, 'This is a post without image', NULL, 1, 0, 0, 0, 0, 4, '2021-01-26 07:22:53', '2021-01-26 07:22:53'),
(18, 46, 'This is a post with an image', NULL, 1, 0, 0, 0, 0, 0, '2021-01-26 07:23:12', '2021-01-26 07:23:12'),
(19, 46, 'asdasdasd', NULL, 1, 0, 0, 0, 0, 0, '2021-01-26 07:54:32', '2021-01-26 07:54:32'),
(20, 46, 'Latest sample post', '1611672973633262.png', 1, 0, 0, 0, 1, 0, '2021-01-26 07:56:13', '2021-01-26 07:56:13'),
(21, 46, 'Asdasdasdasdaaaasdassdadaasda asdasdasdaads', NULL, 1, 0, 0, 1, 1, 0, '2021-01-26 09:39:54', '2021-01-29 02:58:39'),
(22, 46, 'This is the first post for today.', '1611740487227242e46eaeba35786ee8995aeb9018e8cdf3bb', 0, 0, 0, 0, 0, 1, '2021-01-27 02:41:27', '2021-01-28 09:31:17'),
(23, 46, 'hey hey hey', '1611741015e23bae6e2e269b78738005ef8c9c8914105f4321', 0, 0, 0, 0, 2, 0, '2021-01-27 02:50:15', '2021-01-28 05:06:48'),
(24, 46, 'hmmmmmmmmmmmmmmm', '161175697190d79b297dd910ea237aab1663a517aaf7055aa0', 1, 0, 0, 0, 1, 0, '2021-01-27 02:51:45', '2021-01-27 08:57:20'),
(25, 46, 'First share on the microblog', '1611756945166e50bae87176729e7c22f9089e5a9b3eb6165e', 1, 0, 0, 1, 0, 2, '2021-01-27 03:47:59', '2021-01-29 03:23:17'),
(26, 46, 'sample of having modified as sort, now it is edited with picture. Now I am trying to edit the uploaded picture.', '1611757285188b5f5b7da445ab51584ae90789432f70e02490', 0, 0, 0, 0, 0, 0, '2021-01-27 07:17:56', '2021-01-27 09:40:23'),
(27, 46, 'Second try of the share post function', NULL, 1, 0, 0, 0, 0, 0, '2021-01-29 03:28:08', '2021-01-29 03:28:08'),
(28, 46, 'hey hey hyeh ye', NULL, 1, 0, 0, 0, 0, 0, '2021-01-29 06:07:49', '2021-01-29 06:07:49'),
(29, 46, 'hey! what''s up ?', NULL, 1, 0, 0, 1, 0, 0, '2021-01-29 06:27:59', '2021-01-29 06:27:59'),
(30, 46, 'hey! hey! hey!', NULL, 1, 1, 17, 0, 0, 0, '2021-01-29 06:52:47', '2021-01-29 06:52:47'),
(31, 46, 'hahahahaha', NULL, 1, 1, 18, 0, 0, 0, '2021-01-29 06:59:35', '2021-01-29 06:59:35'),
(32, 46, 'nyeeee', NULL, 1, 1, 19, 0, 0, 0, '2021-01-29 08:39:13', '2021-01-29 08:39:13'),
(33, 46, 'This is the first official share post.', NULL, 1, 1, 20, 0, 0, 0, '2021-01-29 08:56:03', '2021-01-29 08:56:03'),
(34, 46, 'First post of the day. Test to see the count of my post.', NULL, 1, 0, NULL, 1, 0, 0, '2021-02-01 03:36:41', '2021-02-01 03:36:41'),
(35, 46, 'First share of the day.', NULL, 1, 1, 21, 0, 1, 1, '2021-02-03 03:41:27', '2021-02-03 03:41:27'),
(36, 47, 'This will be the first post of the newly created account.', NULL, 1, 0, NULL, 0, 0, 0, '2021-02-03 06:47:04', '2021-02-03 06:47:04'),
(37, 47, 'judy', NULL, 0, 0, NULL, 0, 0, 0, '2021-02-04 07:25:35', '2021-02-05 01:30:47'),
(38, 47, '<script>alert(''eeeey'')</script>', NULL, 0, 0, NULL, 0, 0, 0, '2021-02-05 01:28:55', '2021-02-05 01:29:14'),
(39, 47, '<script>alert</script>', NULL, 1, 0, NULL, 0, 0, 0, '2021-02-10 02:18:27', '2021-02-10 02:18:27');

-- --------------------------------------------------------

--
-- Table structure for table `shares`
--

CREATE TABLE IF NOT EXISTS `shares` (
  `id` int(11) NOT NULL,
  `post_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `shares`
--

INSERT INTO `shares` (`id`, `post_id`, `user_id`, `created`, `modified`) VALUES
(17, 29, 46, '2021-01-29 06:52:47', '2021-01-29 06:52:47'),
(18, 21, 46, '2021-01-29 06:59:35', '2021-01-29 06:59:35'),
(19, 25, 46, '2021-01-29 08:39:13', '2021-01-29 08:39:13'),
(20, 3, 46, '2021-01-29 08:56:03', '2021-01-29 08:56:03'),
(21, 34, 46, '2021-02-03 03:41:27', '2021-02-03 03:41:27');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL,
  `activation_key` varchar(255) DEFAULT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0 is not activated, 1 is activated',
  `follow_count` int(11) DEFAULT '0',
  `profile_picture` varchar(255) NOT NULL DEFAULT 'default.png',
  `last_name` varchar(255) NOT NULL,
  `first_name` varchar(255) NOT NULL,
  `birth_date` date NOT NULL,
  `gender` enum('0','1') NOT NULL COMMENT '0 is male, 1 is female',
  `contact_number` varchar(15) NOT NULL,
  `email` varchar(30) NOT NULL,
  `username` varchar(30) NOT NULL,
  `password` varchar(255) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=48 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `activation_key`, `status`, `follow_count`, `profile_picture`, `last_name`, `first_name`, `birth_date`, `gender`, `contact_number`, `email`, `username`, `password`, `created`, `modified`) VALUES
(45, NULL, 1, 1, 'default.png', 'Bredes', 'Judy Ann', '1995-11-18', '0', '09185253488', 'curioussince96@gmail.com', 'judy', '$2a$10$Sxz/Pivrd.Vt4az489WWGed2QJGACq3K0k.nKhUIIGAkDLNXl1zKK', '2021-01-25 01:40:12', '2021-01-25 01:40:12'),
(46, NULL, 1, 1, '1612197908188b5f5b7da445ab51584ae90789432f70e02490', 'Lumanta', 'Gamaliel', '1995-11-18', '0', '09185253489', 'curioussince97@gmail.com', 'yagamishiro', '$2a$10$wbkBZ2mZT7lLW32RvOWufeCdk8RLRQlZ4453XFslPVqCqVhnAiArW', '2021-01-25 01:41:08', '2021-02-02 06:04:21'),
(47, NULL, 1, 1, '16123600595f695caa83314cc76aa88e617aac52ee291b37a9', 'Yagami', 'Shiro', '1995-11-18', '0', '09185253480', 'curioussince94@gmail.com', 'shiroyagami', '$2a$10$p2tfapnDtxROyyjMMueFS.7xEcQzfGg.I1PTdw8owQ9fUgljt0ajC', '2021-02-03 05:19:21', '2021-02-05 06:47:05');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `comments`
--
ALTER TABLE `comments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `follows`
--
ALTER TABLE `follows`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `likes`
--
ALTER TABLE `likes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `posts`
--
ALTER TABLE `posts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `shares`
--
ALTER TABLE `shares`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `comments`
--
ALTER TABLE `comments`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `follows`
--
ALTER TABLE `follows`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=20;
--
-- AUTO_INCREMENT for table `likes`
--
ALTER TABLE `likes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `posts`
--
ALTER TABLE `posts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=40;
--
-- AUTO_INCREMENT for table `shares`
--
ALTER TABLE `shares`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=22;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=48;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
